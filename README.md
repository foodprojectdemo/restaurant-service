[![pipeline status](https://gitlab.com/foodprojectdemo/restaurant-service/badges/main/pipeline.svg)](https://gitlab.com/foodprojectdemo/restaurant-service/-/pipelines)
[![coverage report](https://gitlab.com/foodprojectdemo/restaurant-service/badges/main/coverage.svg)](https://foodprojectdemo.gitlab.io/restaurant-service/coverage/)

# Restaurant service

Restaurant service - a simple microservice for implementing a restaurant bounded context.

The service provides REST for restaurant entities: restaurateur (restaurant owner), restaurant, cuisine, category, dish,
ticket, promo.

The main workflow for the service: getting the ``OrderPlacedEvent`` and creating ticket for cooking. Then a restaurateur
must accept and prepare the ordered food on ticket or reject it.

The microservice applies the Choreography pattern.

### REST API docs

[REST API docs](https://foodprojectdemo.gitlab.io/restaurant-service/docs/)