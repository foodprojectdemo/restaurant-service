package contracts.messaging.events

import org.springframework.cloud.contract.spec.Contract

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

Contract.make {
    label "TicketReadyEvent"
    input {
        triggeredBy("ticketReadyEvent()")
    }
    outputMessage {
        sentTo "events.v1"
        body([
                id         : $(anyUuid()),
                type       : "ticket_ready",
                aggregate  : "ticket",
                aggregateId: $(anyUuid()),
                occurredOn : $(p(iso8601WithOffset()), c(ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT)))
        ])
    }
}
