package contracts.messaging.events

import org.springframework.cloud.contract.spec.Contract

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

Contract.make {
    label "TicketCreatedEvent"
    input {
        triggeredBy("ticketCreatedEvent()")
    }
    outputMessage {
        sentTo "events.v1"
        body([
                id         : $(anyUuid()),
                type       : "ticket_created",
                aggregate  : "ticket",
                aggregateId: $(anyUuid()),
                occurredOn : $(p(iso8601WithOffset()), c(ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT)))
        ])
    }
}
