package contracts.rest.categories

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "Restaurant categories endpoint"
    request {
        method GET()
        url $(regex("/api/v1/restaurants/${positiveInt()}/categories"))
    }
    response {
        status OK()
        headers {
            contentType(applicationJson())
        }
        body([
                [
                        id          : $(anyPositiveInt()),
                        restaurantId: $(anyPositiveInt()),
                        name        : $(anyNonBlankString())
                ]
        ])
    }
}