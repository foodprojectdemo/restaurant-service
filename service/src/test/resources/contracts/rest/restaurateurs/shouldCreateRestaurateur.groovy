package contracts.rest.restaurateurs

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "Restaurateur registration"
    request {
        method POST()
        url "/api/v1/restaurateurs"
        headers {
            contentType(applicationJson())
        }
        body([
                restaurateur: [
                        name    : $(anyNonBlankString()),
                        login   : $(anyEmail()),
                        password: $(anyNonBlankString())
                ],
                restaurant  : [
                        name       : $(anyNonBlankString()),
                        information: $(anyNonBlankString()),
                        logo       : $(anyUrl()),
                        openDays   : $(c(regex("([1-7],?){1,7}")), p('1,2,3,4,5,6,7')),
                        openFrom   : $(regex("[0-9]{2}:[0-9]{2}")),
                        openTo     : $(regex("[0-9]{2}:[0-9]{2}")),
                        legalData  : [
                                address: $(anyNonBlankString()),
                                inn    : $(regex("[0-9]{10,13}")),
                                name   : $(anyNonBlankString()),
                                ogrn   : $(regex("[0-9]{13}")),
                        ]
                ],
                cuisines    : [
                        $(anyNonBlankString())
                ]
        ])
    }
    response {
        status CREATED()
        headers {
            contentType(applicationJson())
        }
        body([
                id: $(anyPositiveInt())
        ])
    }
    priority 1
}