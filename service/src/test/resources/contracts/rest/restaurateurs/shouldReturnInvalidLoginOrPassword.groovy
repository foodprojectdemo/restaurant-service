package contracts.rest.restaurateurs

import org.springframework.cloud.contract.spec.Contract


Contract.make {
    description "Failed authorization"
    request {
        method POST()
        url "/api/v1/restaurateurs/auth"
        headers {
            contentType(applicationJson())
        }
        body([
                login   : $(c(email()), p(anyEmail())),
                password: $(c(anyNonEmptyString()), p(anyNonEmptyString()))
        ])
    }
    response {
        status UNAUTHORIZED()
        headers {
            contentType(applicationJson())
        }
        body([
                error  : 'INVALID_LOGIN_OR_PASSWORD',
                message: 'Invalid login or password'
        ])
    }
    priority 2
}