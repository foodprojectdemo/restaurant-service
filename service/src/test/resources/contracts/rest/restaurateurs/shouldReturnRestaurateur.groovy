package contracts.rest.restaurateurs

import org.springframework.cloud.contract.spec.Contract

final SUCCESSFUL_LOGIN = "successful_login@mail.com"
final SUCCESSFUL_PASSWORD = "password"


Contract.make {
    description "Successful authorization"
    request {
        method POST()
        url "/api/v1/restaurateurs/auth"
        headers {
            contentType(applicationJson())
        }
        body([
                login   : $(SUCCESSFUL_LOGIN),
                password: $(SUCCESSFUL_PASSWORD)
        ])
    }
    response {
        status OK()
        headers {
            contentType(applicationJson())
        }
        body([
                id          : $(anyPositiveInt()),
                restaurantId: $(anyPositiveInt()),
                login       : fromRequest().body('$.login'),
                name        : $(anyNonEmptyString())
        ])
    }
    priority 1
}