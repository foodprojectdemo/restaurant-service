package contracts.rest.restaurants

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "restaurants endpoint"
    request {
        method GET()
        url $(regex("/api/v1/restaurants/${positiveInt()}"))
    }
    response {
        status OK()
        headers {
            contentType(applicationJson())
        }
        body([
                id         : $(anyPositiveInt()),
                name       : $(anyNonBlankString()),
                information: $(anyNonBlankString()),
                logo       : $(anyUrl()),
                openDays   : $(c('1,2,3,4,5,6,7'), p(regex("([1-7],?){1,7}"))),
                openFrom   : $(regex("[0-9]{2}:[0-9]{2}")),
                openTo     : $(regex("[0-9]{2}:[0-9]{2}")),
                legalData  : [
                        name   : $(anyNonEmptyString()),
                        address: $(anyNonEmptyString()),
                        inn    : $(regex("[0-9]{10,13}")),
                        ogrn   : $(regex("[0-9]{13}")),

                ],
                cuisines   : [
                        $(anyNonBlankString())
                ]
        ])
    }
}