package contracts.rest.dishes

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "Restaurant dishes endpoint"
    request {
        method GET()
        url $(regex("/api/v1/restaurants/${positiveInt()}/dishes"))
    }
    response {
        status OK()
        headers {
            contentType(applicationJson())
        }
        body([
                [
                        id          : $(anyPositiveInt()),
                        restaurantId: $(anyPositiveInt()),
                        categoryId  : $(anyPositiveInt()),
                        name        : $(anyNonBlankString()),
                        description : $(anyNonBlankString()),
                        amount      : $(anyPositiveInt()),
                        imageUrl    : $(anyUrl())
                ]
        ])
    }
}