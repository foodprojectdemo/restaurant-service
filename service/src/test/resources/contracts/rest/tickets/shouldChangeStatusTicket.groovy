package contracts.rest.tickets

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "tickets endpoint"
    request {
        method PATCH()
        url $(regex("/api/v1/restaurants/${positiveInt()}/tickets/${uuid()}"))
        headers {
            contentType(applicationJson())
        }
        body([
                status: $(anyOf("ACCEPTED", "REJECTED", "READY"))
        ])
    }
    response {
        status NO_CONTENT()
    }
    priority 1
}