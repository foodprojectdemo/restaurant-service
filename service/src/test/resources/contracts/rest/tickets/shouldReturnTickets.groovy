package contracts.rest.tickets

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "tickets endpoint"
    request {
        method GET()
        url $(regex("/api/v1/restaurants/${positiveInt()}/tickets"))
    }
    response {
        status OK()
        headers {
            contentType(applicationJson())
        }
        body([
                [
                        id          : $(anyUuid()),
                        restaurantId: $(anyPositiveInt()),
                        status      : anyOf("NEW", "ACCEPTED", "REJECTED", "READY"),
                        items       : [[
                                               dishId  : $(anyPositiveInt()),
                                               name    : $(anyNonEmptyString()),
                                               quantity: $(anyPositiveInt())
                                       ]],
                ]
        ])
    }
}