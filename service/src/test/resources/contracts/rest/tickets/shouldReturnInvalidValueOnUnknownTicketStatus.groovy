package contracts.rest.tickets

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "tickets endpoint"
    request {
        method PATCH()
        url $(regex("/api/v1/restaurants/${positiveInt()}/tickets/${uuid()}"))
        headers {
            contentType(applicationJson())
        }
        body([
                status: $(anyNonBlankString())
        ])
    }
    response {
        status BAD_REQUEST()
        headers {
            contentType(applicationJson())
        }
        body([
                message: "Invalid value of status: ${fromRequest().body('$.status')}",
                error  : "INVALID_VALUE"
        ])
        priority 2
    }
}