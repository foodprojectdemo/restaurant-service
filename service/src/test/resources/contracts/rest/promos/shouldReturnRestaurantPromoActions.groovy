package contracts.rest.promos

import org.springframework.cloud.contract.spec.Contract

final PERCENT_PATTERN = "[0-9]{1,2}"

Contract.make {
    description "Restaurant promo actions endpoint"
    request {
        method GET()
        url $(regex("/api/v1/restaurants/${positiveInt()}/promo-actions"))
    }
    response {
        status OK()
        headers {
            contentType(applicationJson())
        }
        body([
                [
                        id          : $(anyPositiveInt()),
                        type        : "absolute_promo_action",
                        restaurantId: $(anyPositiveInt()),
                        discounts   : [
                                [
                                        amount  : $(anyPositiveInt()),
                                        discount: $(anyPositiveInt())
                                ]
                        ]
                ],
                [
                        id          : $(anyPositiveInt()),
                        type        : "percent_promo_action",
                        restaurantId: $(anyPositiveInt()),
                        discounts   : [
                                [
                                        amount : $(anyPositiveInt()),
                                        percent: $(regex(PERCENT_PATTERN))
                                ]
                        ]
                ],
                [
                        id          : $(anyPositiveInt()),
                        type        : "gift_promo_action",
                        restaurantId: $(anyPositiveInt()),
                        gifts   : [
                                [
                                        amount : $(anyPositiveInt()),
                                        dishId: $(regex(PERCENT_PATTERN))
                                ]
                        ]
                ]
        ])
    }
}