package contracts.rest.promos

import org.springframework.cloud.contract.spec.Contract

final PERCENT_PATTERN = "[0-9]{1,2}"

Contract.make {
    description "Percent promo code endpoint"
    request {
        method GET()
        url $(regex("/api/v1/restaurants/${positiveInt()}/promo-codes/PERCENT"))
    }
    response {
        status OK()
        headers {
            contentType(applicationJson())
        }
        body([
                type        : "percent_promo_code",
                code        : "PERCENT",
                restaurantId: $(anyPositiveInt()),
                percent     : $(regex(PERCENT_PATTERN))
        ])
    }
}