package contracts.rest.promos

import org.springframework.cloud.contract.spec.Contract

final PERCENT_PATTERN = "[0-9]{1,2}"

Contract.make {
    description "Absolute promo code endpoint"
    request {
        method GET()
        url $(regex("/api/v1/restaurants/${positiveInt()}/promo-codes/ABSOLUTE"))
    }
    response {
        status OK()
        headers {
            contentType(applicationJson())
        }
        body([
                type        : "absolute_promo_code",
                code        : "ABSOLUTE",
                restaurantId: $(anyPositiveInt()),
                discount    : $(anyPositiveInt())
        ])
    }
}