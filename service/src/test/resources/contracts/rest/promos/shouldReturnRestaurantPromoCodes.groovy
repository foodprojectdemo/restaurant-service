package contracts.rest.promos

import org.springframework.cloud.contract.spec.Contract

final PERCENT_PATTERN = "[0-9]{1,2}"

Contract.make {
    description "Restaurant promo codes endpoint"
    request {
        method GET()
        url $(regex("/api/v1/restaurants/${positiveInt()}/promo-codes"))
    }
    response {
        status OK()
        headers {
            contentType(applicationJson())
        }
        body([
                [
                        type        : "absolute_promo_code",
                        code        : $(anyNonBlankString()),
                        restaurantId: $(anyPositiveInt()),
                        discount    : $(anyPositiveInt())
                ],
                [
                        type        : "percent_promo_code",
                        code        : $(anyNonBlankString()),
                        restaurantId: $(anyPositiveInt()),
                        percent     : $(regex(PERCENT_PATTERN))
                ]
        ])
    }
}