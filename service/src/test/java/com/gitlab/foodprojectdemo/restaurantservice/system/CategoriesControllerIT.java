package com.gitlab.foodprojectdemo.restaurantservice.system;

import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import com.gitlab.foodprojectdemo.restaurantservice.domain.Saved;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.Category;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.CategoryRepository;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.Restaurant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CategoriesControllerIT extends AbstractSystemTest {

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    void shouldCreateCategory(@Saved Restaurant restaurant, Category category) throws Exception {
        var result = mockMvc.perform(
                post("/api/v1/restaurants/{restaurantId}/categories", restaurant.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(category.getInfo()))
        )
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn();

        var id = JsonPath.parse(result.getResponse().getContentAsString()).read("$.id", Long.class);

        assertThat(categoryRepository.findById(id)).isPresent();
    }

    @Test
    void shouldReturnCategories(@Saved Restaurant restaurant, Category category) throws Exception {
        category.setRestaurantId(restaurant.getId());
        category = categoryRepository.save(category);

        mockMvc.perform(get("/api/v1/restaurants/{restaurantId}/categories", category.getRestaurantId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(category.getId()), Long.class));
    }
}
