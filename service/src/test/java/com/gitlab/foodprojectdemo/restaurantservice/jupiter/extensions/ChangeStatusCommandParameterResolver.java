package com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.ChangeStatusCommand;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.NewStatus;
import com.gitlab.foodprojectdemo.restaurantservice.fixture.OrderFixture;
import com.gitlab.foodprojectdemo.restaurantservice.fixture.RestaurantFixture;

public class ChangeStatusCommandParameterResolver extends TypeBasedParameterResolver<ChangeStatusCommand> {

    @Override
    public ChangeStatusCommand resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return new ChangeStatusCommand(OrderFixture.orderId(), RestaurantFixture.restaurantId(), NewStatus.ACCEPTED);
    }
}
