package com.gitlab.foodprojectdemo.restaurantservice.adapter.persistence;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.AbsoluteDiscountPromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.GiftPromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PercentDiscountPromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoActionRepository;

import static org.assertj.core.api.Assertions.assertThat;

@RequiredArgsConstructor
class JpaPromoActionRepositoryIT extends SimpleJpaDomainRepositoryIT<PromoActionRepository, PromoAction, Long> {

    @Getter
    private final PromoAction aggregate;

    @Getter
    private final Class<? extends PromoActionRepository> jpaRepositoryClass = JpaPromoActionRepository.class;

    @Getter
    @Autowired
    private PromoActionRepository repository;

    @Test
    void shouldSaveAndFind(AbsoluteDiscountPromoAction absoluteDiscountPromoAction) {
        shouldSaveAndFind((PromoAction) absoluteDiscountPromoAction);
    }

    @Test
    void shouldSaveAndFind(PercentDiscountPromoAction percentDiscountPromoAction) {
        shouldSaveAndFind((PromoAction) percentDiscountPromoAction);
    }

    @Test
    void shouldSaveAndFind(GiftPromoAction giftPromoAction) {
        shouldSaveAndFind((PromoAction) giftPromoAction);
    }

    @Test
    void findByRestaurantId(PromoAction promoAction) {
        repository.save(promoAction);
        assertThat(repository.findByRestaurantId(promoAction.getRestaurantId())).contains(promoAction);
    }

}