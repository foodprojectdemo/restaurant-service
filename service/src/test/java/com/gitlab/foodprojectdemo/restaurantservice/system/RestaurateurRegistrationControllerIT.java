package com.gitlab.foodprojectdemo.restaurantservice.system;

import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.Cuisine;
import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.CuisineRepository;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RegistrationService.RegistrationCommand;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.Restaurant;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantRepository;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.Restaurateur;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.RestaurateurRepository;

import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RestaurateurRegistrationControllerIT extends AbstractSystemTest {

    @Autowired
    private RestaurateurRepository restaurateurRepository;

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Autowired
    private CuisineRepository cuisineRepository;

    @Test
    void shouldRegisterRestaurateurWithRestaurant(Restaurateur restaurateur, Restaurant restaurant) throws Exception {
        Set<String> cuisines = restaurant.getCuisines().stream().map(Cuisine::getName).collect(Collectors.toSet());
        var command = new RegistrationCommand(restaurateur.getInfo(), restaurant.getInfo(), cuisines);

        var result = mockMvc.perform(
                post("/api/v1/restaurateurs")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(command))
        )
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn();

        var id = JsonPath.parse(result.getResponse().getContentAsString()).read("$.id", Long.class);

        var restaurateurOptional = restaurateurRepository.findById(id);
        assertThat(restaurateurOptional).isPresent();
        var createdRestaurateur = restaurateurOptional.get();

        assertThat(restaurantRepository.findById(createdRestaurateur.getRestaurantId())).isPresent();
    }
}
