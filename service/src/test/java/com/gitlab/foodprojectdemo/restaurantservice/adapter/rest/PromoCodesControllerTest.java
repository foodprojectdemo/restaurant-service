package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.AbsoluteDiscountPromoCode;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PercentDiscountPromoCode;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoCodeService;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.Restaurant;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.PromoCodeFixture.absoluteDiscountPromoCode;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.PromoCodeFixture.percentDiscountPromoCode;

@WebMvcTest(PromoCodesController.class)
class PromoCodesControllerTest extends AbstractRestTest {

    @MockBean
    private PromoCodeService promoCodeService;

    @Test
    void shouldReturnPromoCodes(Restaurant restaurant) throws Exception {
        var absoluteDiscountPromoCode = absoluteDiscountPromoCode(restaurant.getId());
        var percentDiscountPromoCode = percentDiscountPromoCode(restaurant.getId());
        var promoActions = List.of(
                absoluteDiscountPromoCode,
                percentDiscountPromoCode
        );
        when(promoCodeService.promoCodes(restaurant.getId())).thenReturn(promoActions);

        mockMvc.perform(get("/api/v1/restaurants/{restaurantId}/promo-codes", restaurant.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].restaurantId", is(restaurant.getId()), Long.class))
                .andExpect(jsonPath("$[0].code", is(absoluteDiscountPromoCode.getCode())))
                .andExpect(jsonPath("$[0].type", is("absolute_promo_code")))
                .andExpect(jsonPath("$[0].discount", is(absoluteDiscountPromoCode.getDiscount())))
                .andExpect(jsonPath("$[1].restaurantId", is(restaurant.getId()), Long.class))
                .andExpect(jsonPath("$[1].code", is(percentDiscountPromoCode.getCode())))
                .andExpect(jsonPath("$[1].type", is("percent_promo_code")))
                .andExpect(jsonPath("$[1].percent", is(percentDiscountPromoCode.getPercent())))
                .andDo(document("get-promo-codes"));
    }

    @Test
    void shouldReturnAbsoluteDiscountPromoCode(AbsoluteDiscountPromoCode promoCode) throws Exception {
        when(promoCodeService.promoCode(promoCode.getRestaurantId(), promoCode.getCode())).thenReturn(Optional.of(promoCode));

        mockMvc.perform(get("/api/v1/restaurants/{restaurantId}/promo-codes/{code}", promoCode.getRestaurantId(), promoCode.getCode()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.restaurantId", is(promoCode.getRestaurantId()), Long.class))
                .andExpect(jsonPath("$.code", is(promoCode.getCode())))
                .andExpect(jsonPath("$.type", is("absolute_promo_code")))
                .andExpect(jsonPath("$.discount", is(promoCode.getDiscount())))
                .andDo(document("get-absolute-discount-promo-code"));
    }

    @Test
    void shouldReturnPercentPromoCode(PercentDiscountPromoCode promoCode) throws Exception {
        when(promoCodeService.promoCode(promoCode.getRestaurantId(), promoCode.getCode())).thenReturn(Optional.of(promoCode));

        mockMvc.perform(get("/api/v1/restaurants/{restaurantId}/promo-codes/{code}", promoCode.getRestaurantId(), promoCode.getCode()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.restaurantId", is(promoCode.getRestaurantId()), Long.class))
                .andExpect(jsonPath("$.code", is(promoCode.getCode())))
                .andExpect(jsonPath("$.type", is("percent_promo_code")))
                .andExpect(jsonPath("$.percent", is(promoCode.getPercent())))
                .andDo(document("get-percent-discount-promo-code"));
    }

}