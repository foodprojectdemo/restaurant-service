package com.gitlab.foodprojectdemo.restaurantservice.fixture;

import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.Dish;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.DishInfo;

import static com.gitlab.foodprojectdemo.restaurantservice.Faker.faker;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.IdFixture.id;

public class DishFixture {

    public static Dish dish() {
        return new Dish(
                id(),
                id(),
                id(),
                dishInfo()
        );
    }

    public static DishInfo dishInfo() {
        return DishInfo.builder()
                .name(faker().food().dish())
                .description(faker().lorem().paragraph())
                .amount(faker().number().numberBetween(1, 1000))
                .imageUrl(faker().internet().image())
                .build();
    }
}
