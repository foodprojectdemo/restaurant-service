package com.gitlab.foodprojectdemo.restaurantservice.adapter.persistence;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.gitlab.foodprojectdemo.restaurantservice.domain.Saved;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.Category;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.CategoryRepository;

import static org.assertj.core.api.Assertions.assertThat;

@RequiredArgsConstructor
class JpaCategoryRepositoryIT  extends SimpleJpaDomainRepositoryIT<CategoryRepository, Category, Long> {

    @Getter
    private final Category aggregate;

    @Getter
    private final Class<? extends CategoryRepository> jpaRepositoryClass = JpaCategoryRepository.class;

    @Getter
    @Autowired
    private CategoryRepository repository;

    @Test
    void shouldFindByRestaurantId(@Saved Category category) {
        assertThat(repository.findByRestaurantId(category.getRestaurantId()).size()).isEqualTo(1);
    }

}