package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.Category;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.CategoryService;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantNotFoundException;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CategoriesController.class)
class CategoriesControllerTest extends AbstractRestTest {
    private static final String URI = "/api/v1/restaurants/{restaurantId}/categories";

    @MockBean
    private CategoryService categoryService;

    @Test
    void shouldCreateCategory(Category category) throws Exception {
        when(categoryService.addCategory(category.getRestaurantId(), category.getInfo())).thenReturn(category);

        mockMvc.perform(
                RestDocumentationRequestBuilders.post(URI, category.getRestaurantId())
                        .content(objectMapper.writeValueAsString(category.getInfo()))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(category.getId()), Long.class))
                .andDo(document("create-category"));
    }

    @Test
    void shouldReturnRestaurantNotFoundError(Category category) throws Exception {
        when(categoryService.addCategory(category.getRestaurantId(), category.getInfo()))
                .thenThrow(new RestaurantNotFoundException(category.getRestaurantId()));

        mockMvc.perform(
                post(URI, category.getRestaurantId())
                        .content(objectMapper.writeValueAsString(category.getInfo()))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message", is(String.format("Restaurant with ID %d was not found", category.getRestaurantId()))))
                .andExpect(jsonPath("$.error", is("NOT_FOUND")));
    }

    @Test
    void shouldReturnCategories(Category category) throws Exception {
        when(categoryService.categories(category.getRestaurantId())).thenReturn(List.of(category));

        mockMvc.perform(get(URI, category.getRestaurantId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is(category.getId()), Long.class))
                .andExpect(jsonPath("$[0].restaurantId", is(category.getRestaurantId()), Long.class))
                .andExpect(jsonPath("$[0].name", is(category.getName())))
                .andDo(document("get-categories"));

    }


}