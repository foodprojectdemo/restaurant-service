package com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.AbsoluteDiscountPromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.fixture.PromoActionFixture;

public class AbsoluteDiscountPromoActionParameterResolver extends TypeBasedParameterResolver<AbsoluteDiscountPromoAction> {

    @Override
    public AbsoluteDiscountPromoAction resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return PromoActionFixture.absoluteDiscountPromoAction();
    }
}
