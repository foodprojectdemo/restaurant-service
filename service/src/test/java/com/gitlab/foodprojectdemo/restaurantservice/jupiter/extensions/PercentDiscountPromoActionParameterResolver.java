package com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PercentDiscountPromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.fixture.PromoActionFixture;

public class PercentDiscountPromoActionParameterResolver extends TypeBasedParameterResolver<PercentDiscountPromoAction> {

    @Override
    public PercentDiscountPromoAction resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return PromoActionFixture.percentDiscountPromoAction();
    }
}
