package com.gitlab.foodprojectdemo.restaurantservice.adapter.json;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import com.gitlab.foodprojectdemo.restaurantservice.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PercentDiscountPromoAction;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@JsonTest
class PercentDiscountPromoActionJsonTest extends AbstractTest {

    @Autowired
    private JacksonTester<PercentDiscountPromoAction> tester;

    @Test
    void serialize(PercentDiscountPromoAction promoAction) throws IOException {
        var json = tester.write(promoAction);
        assertThat(json).extractingJsonPathStringValue("$.type").isEqualTo("percent_promo_action");
        assertThat(json).extractingJsonPathNumberValue("$.id").isEqualTo(promoAction.getId());
        assertThat(json).extractingJsonPathNumberValue("$.restaurantId").isEqualTo(promoAction.getRestaurantId());
        assertThat(json).extractingJsonPathNumberValue("$.discounts[0].amount").isEqualTo(promoAction.getDiscounts().get(0).getAmount().longValue());
        assertThat(json).extractingJsonPathNumberValue("$.discounts[0].percent").isEqualTo(promoAction.getDiscounts().get(0).getPercent().longValue());
        assertThat(json).extractingJsonPathNumberValue("$.discounts[1].amount").isEqualTo(promoAction.getDiscounts().get(1).getAmount().longValue());
        assertThat(json).extractingJsonPathNumberValue("$.discounts[1].percent").isEqualTo(promoAction.getDiscounts().get(1).getPercent().longValue());
    }

    @Test
    void deserialize(PercentDiscountPromoAction promoAction) throws IOException {
        var json = "{\"type\":\"percent_promo_action\"," +
                "\"id\":\"" + promoAction.getId().toString() + "\"," +
                "\"restaurantId\":\"" + promoAction.getRestaurantId().toString() + "\"," +
                "\"discounts\": [{\"amount\": " + promoAction.getDiscounts().get(0).getAmount() + ",\"percent\": " + promoAction.getDiscounts().get(0).getPercent() + "}]}";

        log.info(json);

        var actual = tester.parse(json).getObject();
    }

}