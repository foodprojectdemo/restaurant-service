package com.gitlab.foodprojectdemo.restaurantservice.adapter.json;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import com.gitlab.foodprojectdemo.restaurantservice.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainEventId;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderPlacedEvent;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.UUID;

@Slf4j
@JsonTest
class OrderPlacedEventJsonTest extends AbstractTest {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private JacksonTester<OrderPlacedEvent> tester;

    @Test
    void serialize() throws IOException {
        var orderPlacedEvent = new OrderPlacedEvent(
                new DomainEventId(),
                "order",
                new OrderId(UUID.randomUUID().toString()),
                ZonedDateTime.now()
        );

        log.info("{}", tester.write(orderPlacedEvent).getJson());
    }

    @Test
    void deserialize() throws IOException {
        var json = "{\"type\":\"order_placed\"," +
                "\"id\":\"299f119f-0613-4d23-8be0-4aea518ad5f9\"," +
                "\"aggregate\":\"order\"," +
                "\"aggregateId\":\"619bfba4-be69-4d1f-9e34-e41d45f68bd1\"," +
                "\"occurredOn\":\"2021-04-02T22:17:59.749335+05:00\"}";

        var orderPlacedEvent = tester.parse(json).getObject();
    }
}