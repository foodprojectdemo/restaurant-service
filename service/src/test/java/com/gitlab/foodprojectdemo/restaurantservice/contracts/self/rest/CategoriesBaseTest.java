package com.gitlab.foodprojectdemo.restaurantservice.contracts.self.rest;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.context.WebApplicationContext;
import com.gitlab.foodprojectdemo.restaurantservice.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantservice.adapter.rest.CategoriesController;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.Category;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.CategoryService;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest(CategoriesController.class)
public class CategoriesBaseTest extends AbstractTest {

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private CategoryService categoryService;

    @BeforeEach
    void setUp(Category category) {
        when(categoryService.categories(any())).thenReturn(List.of(category));

        RestAssuredMockMvc.webAppContextSetup(context);
    }

}
