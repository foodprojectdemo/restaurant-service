package com.gitlab.foodprojectdemo.restaurantservice.fixture;

import static com.gitlab.foodprojectdemo.restaurantservice.Faker.faker;

public class IdFixture {
    private static final long MIN = 1L;
    private static final long MAX = 1000L;

    public static Long id() {
        return faker().number().numberBetween(MIN, MAX);
    }
}
