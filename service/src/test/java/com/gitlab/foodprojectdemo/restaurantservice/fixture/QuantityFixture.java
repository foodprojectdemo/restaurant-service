package com.gitlab.foodprojectdemo.restaurantservice.fixture;

import static com.gitlab.foodprojectdemo.restaurantservice.Faker.faker;

public class QuantityFixture {

    public static int quantity() {
        return faker().number().numberBetween(1, 1000);
    }

}
