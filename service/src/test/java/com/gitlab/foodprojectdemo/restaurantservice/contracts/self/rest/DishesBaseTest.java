package com.gitlab.foodprojectdemo.restaurantservice.contracts.self.rest;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.web.context.WebApplicationContext;
import com.gitlab.foodprojectdemo.restaurantservice.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantservice.adapter.rest.DishesController;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.Dish;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.DishService;

import java.util.Collections;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest(controllers = {
        DishesController.class
})
@AutoConfigureMessageVerifier
public class DishesBaseTest extends AbstractTest {

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private DishService dishService;

    @BeforeEach
    void setUp(Dish dish) {
        when(dishService.dishes(any())).thenReturn(Collections.singletonList(dish));
        when(dishService.dish(any())).thenReturn(Optional.of(dish));

        RestAssuredMockMvc.webAppContextSetup(context);
    }

}
