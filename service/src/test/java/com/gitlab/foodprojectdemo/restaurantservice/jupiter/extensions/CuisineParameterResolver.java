package com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.gitlab.foodprojectdemo.restaurantservice.domain.Saved;
import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.Cuisine;
import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.CuisineRepository;
import com.gitlab.foodprojectdemo.restaurantservice.fixture.CuisineFixture;

public class CuisineParameterResolver extends TypeBasedParameterResolver<Cuisine> {

    @Override
    public Cuisine resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        var cuisine = CuisineFixture.cuisine();
        if (parameterContext.isAnnotated(Saved.class)) {
            var applicationContext = SpringExtension.getApplicationContext(extensionContext);
            var cuisineRepository = applicationContext.getBean(CuisineRepository.class);
            cuisineRepository.save(cuisine);
        }
        return cuisine;
    }
}
