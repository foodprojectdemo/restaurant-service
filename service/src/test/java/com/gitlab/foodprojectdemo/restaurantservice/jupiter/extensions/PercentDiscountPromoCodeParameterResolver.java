package com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PercentDiscountPromoCode;
import com.gitlab.foodprojectdemo.restaurantservice.fixture.PromoCodeFixture;

public class PercentDiscountPromoCodeParameterResolver extends TypeBasedParameterResolver<PercentDiscountPromoCode> {

    @Override
    public PercentDiscountPromoCode resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return PromoCodeFixture.percentDiscountPromoCode();
    }
}
