package com.gitlab.foodprojectdemo.restaurantservice.contracts.self.rest;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.web.context.WebApplicationContext;
import com.gitlab.foodprojectdemo.restaurantservice.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantservice.adapter.rest.PromoActionsController;
import com.gitlab.foodprojectdemo.restaurantservice.adapter.rest.PromoCodesController;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.AbsoluteDiscountPromoCode;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PercentDiscountPromoCode;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoActionService;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoCodeService;

import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static com.gitlab.foodprojectdemo.restaurantservice.Faker.faker;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.IdFixture.id;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.PromoActionFixture.absoluteDiscountPromoAction;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.PromoActionFixture.giftPromoAction;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.PromoActionFixture.percentDiscountPromoAction;

@WebMvcTest(controllers = {
        PromoActionsController.class,
        PromoCodesController.class
})
@AutoConfigureMessageVerifier
public class PromosBaseTest extends AbstractTest {

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private PromoActionService promoActionService;

    @MockBean
    private PromoCodeService promoCodeService;

    @BeforeEach
    void setUp() {
        mockPromoActionService();
        mockPromoCodeService();

        RestAssuredMockMvc.webAppContextSetup(context);
    }


    private void mockPromoActionService() {
        var promoActions = List.of(
                absoluteDiscountPromoAction(),
                percentDiscountPromoAction(),
                giftPromoAction()
        );
        when(promoActionService.promoActions(any())).thenAnswer(__ -> promoActions);
    }

    private void mockPromoCodeService() {
        var restaurantId = id();
        var promoCodes = List.of(
                new AbsoluteDiscountPromoCode(faker().lorem().word(), restaurantId, faker().number().numberBetween(100, 5000)),
                new PercentDiscountPromoCode(faker().lorem().word(), restaurantId, faker().number().numberBetween(1, 99))
        );

        when(promoCodeService.promoCodes(any())).thenReturn(promoCodes);
        when(promoCodeService.promoCode(any(), eq("ABSOLUTE"))).thenReturn(Optional.of(
                new AbsoluteDiscountPromoCode("ABSOLUTE", restaurantId, faker().number().numberBetween(100, 5000))));
        when(promoCodeService.promoCode(any(), eq("PERCENT"))).thenReturn(Optional.of(
                new PercentDiscountPromoCode("PERCENT", restaurantId, faker().number().numberBetween(1, 99))));
    }
}
