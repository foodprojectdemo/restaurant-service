package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.Cuisine;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.Restaurant;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantService;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RestaurantsController.class)
class RestaurantsControllerTest extends AbstractRestTest {

    @MockBean
    private RestaurantService restaurantService;

    @Test
    void shouldReturnRestaurants(Restaurant restaurant) throws Exception {
        when(restaurantService.restaurants()).thenReturn(List.of(restaurant));

        mockMvc.perform(get("/api/v1/restaurants"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(restaurant.getId()), Long.class))
                .andExpect(jsonPath("$[0].name", is(restaurant.getName())))
                .andExpect(jsonPath("$[0].information", is(restaurant.getInformation())))
                .andExpect(jsonPath("$[0].logo", is(restaurant.getLogo())))
                .andExpect(jsonPath("$[0].openDays", is(restaurant.getOpenDays())))
                .andExpect(jsonPath("$[0].openFrom", is(restaurant.getOpenFrom())))
                .andExpect(jsonPath("$[0].openTo", is(restaurant.getOpenTo())))
                .andExpect(jsonPath("$[0].legalData.name", is(restaurant.getLegalData().getName())))
                .andExpect(jsonPath("$[0].legalData.address", is(restaurant.getLegalData().getAddress())))
                .andExpect(jsonPath("$[0].legalData.inn", is(restaurant.getLegalData().getInn())))
                .andExpect(jsonPath("$[0].legalData.ogrn", is(restaurant.getLegalData().getOgrn())))
                .andExpect(jsonPath("$[0].cuisines", containsInAnyOrder(cuisinesToTheirNames(restaurant.getCuisines()))))
                .andDo(document("get-restaurants"));

    }

    @Test
    void shouldReturnRestaurant(Restaurant restaurant) throws Exception {
        when(restaurantService.restaurant(restaurant.getId())).thenReturn(Optional.of(restaurant));

        mockMvc.perform(get("/api/v1/restaurants/{restaurantId}", restaurant.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(restaurant.getId()), Long.class))
                .andExpect(jsonPath("$.name", is(restaurant.getName())))
                .andExpect(jsonPath("$.information", is(restaurant.getInformation())))
                .andExpect(jsonPath("$.logo", is(restaurant.getLogo())))
                .andExpect(jsonPath("$.openDays", is(restaurant.getOpenDays())))
                .andExpect(jsonPath("$.openFrom", is(restaurant.getOpenFrom())))
                .andExpect(jsonPath("$.openTo", is(restaurant.getOpenTo())))
                .andExpect(jsonPath("$.legalData.name", is(restaurant.getLegalData().getName())))
                .andExpect(jsonPath("$.legalData.address", is(restaurant.getLegalData().getAddress())))
                .andExpect(jsonPath("$.legalData.inn", is(restaurant.getLegalData().getInn())))
                .andExpect(jsonPath("$.legalData.ogrn", is(restaurant.getLegalData().getOgrn())))
                .andExpect(jsonPath("$.cuisines", containsInAnyOrder(cuisinesToTheirNames(restaurant.getCuisines()))))
                .andDo(document("get-restaurant"));
    }


    private Object[] cuisinesToTheirNames(Collection<Cuisine> cuisines) {
        return cuisines.stream().map(Cuisine::getName).toArray();
    }
}