package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.Dish;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.DishService;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DishesController.class)
class RestaurantDishesControllerTest extends AbstractRestTest {

    @MockBean
    private DishService dishService;

    @Test
    void shouldReturnRestaurantDishes(Dish dish) throws Exception {
        when(dishService.dishes(dish.getRestaurantId())).thenReturn(List.of(dish));

        mockMvc.perform(get("/api/v1/restaurants/{restaurantId}/dishes", dish.getRestaurantId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(dish.getId()), Long.class))
                .andExpect(jsonPath("$[0].restaurantId", is(dish.getRestaurantId()), Long.class))
                .andExpect(jsonPath("$[0].name", is(dish.getName())))
                .andExpect(jsonPath("$[0].description", is(dish.getDescription())))
                .andExpect(jsonPath("$[0].amount", is(dish.getAmount())))
                .andExpect(jsonPath("$[0].imageUrl", is(dish.getImageUrl())))
                .andDo(document("get-restaurant-dishes"));

    }

}