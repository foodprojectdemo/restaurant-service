package com.gitlab.foodprojectdemo.restaurantservice.fixture;

import com.gitlab.foodprojectdemo.restaurantservice.domain.category.Category;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.CategoryInfo;

import static com.gitlab.foodprojectdemo.restaurantservice.Faker.faker;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.IdFixture.id;

public class CategoryFixture {

    public static Category category() {
        return new Category(
                id(),
                id(),
                categoryInfo()
        );
    }

    public static CategoryInfo categoryInfo() {
        return new CategoryInfo(faker().food().dish());
    }
}
