package com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.gitlab.foodprojectdemo.restaurantservice.domain.Saved;
import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.CuisineRepository;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.Restaurant;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantRepository;
import com.gitlab.foodprojectdemo.restaurantservice.fixture.RestaurantFixture;

import java.util.Set;

import static com.gitlab.foodprojectdemo.restaurantservice.fixture.CuisineFixture.cuisine;

public class RestaurantParameterResolver extends TypeBasedParameterResolver<Restaurant> {

    @Override
    public Restaurant resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        var cuisine = cuisine();
        var restaurant = RestaurantFixture.restaurant(Set.of(cuisine));
        if (parameterContext.isAnnotated(Saved.class)) {
            var applicationContext = SpringExtension.getApplicationContext(extensionContext);
            var restaurantRepository = applicationContext.getBean(RestaurantRepository.class);
            var cuisineRepository = applicationContext.getBean(CuisineRepository.class);
            cuisineRepository.save(cuisine);
            restaurantRepository.save(restaurant);
        }
        return restaurant;
    }
}
