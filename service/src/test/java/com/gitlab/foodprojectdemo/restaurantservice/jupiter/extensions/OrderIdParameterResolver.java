package com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;
import com.gitlab.foodprojectdemo.restaurantservice.fixture.OrderFixture;

public class OrderIdParameterResolver extends TypeBasedParameterResolver<OrderId> {
    @Override
    public OrderId resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return OrderFixture.orderId();
    }
}
