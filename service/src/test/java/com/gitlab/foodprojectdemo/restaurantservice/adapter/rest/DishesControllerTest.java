package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.Dish;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.DishService;

import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DishesController.class)
class DishesControllerTest extends AbstractRestTest {

    @MockBean
    private DishService dishService;

    @Test
    void shouldCreateDish(Dish dish) throws Exception {
        when(dishService.addDish(dish.getRestaurantId(), dish.getCategoryId(), dish.getInfo())).thenReturn(dish);

        mockMvc.perform(
                post("/api/v1/restaurants/{restaurantId}/categories/{categoryId}/dishes",
                        dish.getRestaurantId(),
                        dish.getCategoryId()
                )
                        .content(objectMapper.writeValueAsString(dish.getInfo()))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(dish.getId()), Long.class))
                .andDo(document("create-dish"));
    }

    @Test
    void shouldReturnDish(Dish dish) throws Exception {
        when(dishService.dish(dish.getId())).thenReturn(Optional.of(dish));

        mockMvc.perform(get("/api/v1/dishes/{dishId}", dish.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", CoreMatchers.is(dish.getId()), Long.class))
                .andExpect(jsonPath("$.restaurantId", CoreMatchers.is(dish.getRestaurantId()), Long.class))
                .andExpect(jsonPath("$.name", CoreMatchers.is(dish.getName())))
                .andExpect(jsonPath("$.description", CoreMatchers.is(dish.getDescription())))
                .andExpect(jsonPath("$.amount", CoreMatchers.is(dish.getAmount())))
                .andExpect(jsonPath("$.imageUrl", CoreMatchers.is(dish.getImageUrl())))
                .andDo(document("get-dish"));
    }
}