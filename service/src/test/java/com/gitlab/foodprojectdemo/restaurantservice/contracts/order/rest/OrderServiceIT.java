package com.gitlab.foodprojectdemo.restaurantservice.contracts.order.rest;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import com.gitlab.foodprojectdemo.restaurantservice.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantservice.adapter.gateway.order.OrderGateway;
import com.gitlab.foodprojectdemo.restaurantservice.config.WebClientConfig;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderRepository;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@SpringBootTest
@AutoConfigureWireMock(
        stubs = "classpath:/META-INF/com.gitlab.foodprojectdemo/order-service/**/mappings/**/*.json",
        port = 0)
@TestPropertySource(properties = "order-service.url=http://localhost:${wiremock.server.port}/")
@ContextConfiguration(classes = {OrderGateway.class,  WebClientConfig.class})
class OrderServiceIT extends AbstractTest {

    @Autowired
    private OrderRepository orderRepository;

    @Test
    void shouldReturnOrder(OrderId orderId) {
        var order = orderRepository.findById(orderId);
        assertThat(order).isPresent();
    }
}
