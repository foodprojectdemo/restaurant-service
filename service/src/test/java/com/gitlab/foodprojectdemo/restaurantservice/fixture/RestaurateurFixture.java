package com.gitlab.foodprojectdemo.restaurantservice.fixture;

import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.Restaurateur;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.RestaurateurInfo;

import static com.gitlab.foodprojectdemo.restaurantservice.Faker.faker;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.IdFixture.id;

public class RestaurateurFixture {

    public static Restaurateur restaurateur() {
        return new Restaurateur(
                id(),
                id(),
                restaurateurInfo()
        );
    }

    public static RestaurateurInfo restaurateurInfo() {
        return new RestaurateurInfo(
                faker().name().fullName(),
                faker().internet().emailAddress(),
                faker().internet().password()
        );
    }
}
