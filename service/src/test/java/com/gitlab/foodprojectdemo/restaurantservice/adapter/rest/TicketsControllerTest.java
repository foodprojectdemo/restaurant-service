package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.Restaurant;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.ChangeStatusCommand;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.NewStatus;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.Ticket;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.TicketService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.snippet.Attributes.key;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static com.gitlab.foodprojectdemo.restaurantservice.Faker.faker;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.RestaurantFixture.restaurantId;

@WebMvcTest(TicketsController.class)
class TicketsControllerTest extends AbstractRestTest {

    @MockBean
    private TicketService ticketService;

    @Test
    void tickets(Ticket ticket) throws Exception {
        var restaurantId = restaurantId();
        when(ticketService.tickets(restaurantId)).thenReturn(List.of(ticket));

        mockMvc.perform(get("/api/v1/restaurants/{restaurantId}/tickets", restaurantId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(ticket.getId().toString())))
                .andExpect(jsonPath("$[0].status", is(ticket.getStatus().toString())))
                .andExpect(jsonPath("$[0].items[0].dishId", is(ticket.getItems().get(0).getDishId()), Long.class))
                .andExpect(jsonPath("$[0].items[0].name", is(ticket.getItems().get(0).getName())))
                .andExpect(jsonPath("$[0].items[0].quantity", is(ticket.getItems().get(0).getQuantity())))
                .andDo(document("get-tickets"));

        verify(ticketService).tickets(restaurantId);
    }

    @ParameterizedTest
    @EnumSource(NewStatus.class)
    void changeStatus(NewStatus status, Restaurant restaurant, OrderId orderId) throws Exception {
        var json = objectMapper.writeValueAsString(Map.of("status", status));
        mockMvc.perform(patch("/api/v1/restaurants/{restaurantId}/tickets/{orderId}", restaurant.getId(), orderId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andDo(print())
                .andExpect(status().isNoContent())
                .andDo(documentChangeStatus());

        verify(ticketService).changeStatus(new ChangeStatusCommand(orderId, restaurant.getId(), status));
    }

    private RestDocumentationResultHandler documentChangeStatus() {
        return document("change-ticket-status",
                requestFields(
                        fieldWithPath("status")
                                .description("New ticket status")
                                .attributes(key("constraints").value(String.format("Possible values: %s", Arrays.toString(NewStatus.values())))
                                ))
        );
    }

    @Test
    void shouldReturnBadRequestWhenUnknownStatus(Restaurant restaurant, OrderId orderId) throws Exception {
        var status = faker().lorem().word();
        var json = objectMapper.writeValueAsString(Map.of("status", status));
        mockMvc.perform(patch("/api/v1/restaurants/{restaurantId}/tickets/{orderId}", restaurant.getId(), orderId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("Invalid value of status: " + status)))
                .andExpect(jsonPath("$.error", is("INVALID_VALUE")))
                .andDo(document("invalid-change-ticket-status"));

    }

}