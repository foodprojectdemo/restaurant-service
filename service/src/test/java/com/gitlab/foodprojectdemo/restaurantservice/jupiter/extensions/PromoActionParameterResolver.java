package com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.fixture.PromoActionFixture;

public class PromoActionParameterResolver extends TypeBasedParameterResolver<PromoAction> {

    @Override
    public PromoAction resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return PromoActionFixture.promoAction();
    }
}
