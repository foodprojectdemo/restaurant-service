package com.gitlab.foodprojectdemo.restaurantservice.adapter.persistence;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.gitlab.foodprojectdemo.restaurantservice.domain.Saved;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.Ticket;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.TicketRepository;

import static org.assertj.core.api.Assertions.assertThat;

@RequiredArgsConstructor
class JpaTicketRepositoryIT extends SimpleJpaDomainRepositoryIT<TicketRepository, Ticket, OrderId> {

    @Getter
    private final Ticket aggregate;

    @Getter
    private final Class<? extends TicketRepository> jpaRepositoryClass = JpaTicketRepository.class;

    @Getter
    @Autowired
    private TicketRepository repository;

    @Test
    void findByRestaurantId(@Saved Ticket ticket) {
        assertThat(repository.findByRestaurantId(ticket.getRestaurantId())).contains(ticket);
    }
}