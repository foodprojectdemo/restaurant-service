package com.gitlab.foodprojectdemo.restaurantservice.contracts.self.rest;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.context.WebApplicationContext;
import com.gitlab.foodprojectdemo.restaurantservice.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantservice.adapter.rest.TicketsController;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.Ticket;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.TicketService;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest(TicketsController.class)
public class TicketsBaseTest extends AbstractTest {

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private TicketService ticketService;

    @BeforeEach
    void setUp(Ticket ticket) {
        when(ticketService.tickets(any())).thenReturn(List.of(ticket));

        RestAssuredMockMvc.webAppContextSetup(context);
    }

}
