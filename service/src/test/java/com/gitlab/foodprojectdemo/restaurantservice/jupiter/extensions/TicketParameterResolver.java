package com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.gitlab.foodprojectdemo.restaurantservice.domain.Saved;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.Ticket;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.TicketRepository;
import com.gitlab.foodprojectdemo.restaurantservice.fixture.TicketFixture;

public class TicketParameterResolver extends TypeBasedParameterResolver<Ticket> {

    @Override
    public Ticket resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        var ticket = TicketFixture.newTicket();
        if (parameterContext.isAnnotated(Saved.class)) {
            var applicationContext = SpringExtension.getApplicationContext(extensionContext);
            var ticketRepository = applicationContext.getBean(TicketRepository.class);
            ticketRepository.save(ticket);
        }
        return ticket;
    }
}
