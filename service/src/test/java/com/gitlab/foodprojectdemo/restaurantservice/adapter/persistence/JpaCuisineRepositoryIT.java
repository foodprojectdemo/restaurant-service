package com.gitlab.foodprojectdemo.restaurantservice.adapter.persistence;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.Cuisine;
import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.CuisineRepository;

import java.util.ArrayList;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.CuisineFixture.cuisine;

@RequiredArgsConstructor
@Import(CuisineRepositoryImpl.class)
class JpaCuisineRepositoryIT extends SimpleJpaDomainRepositoryIT<CuisineRepository, Cuisine, Long> {

    @Getter
    private final Cuisine aggregate;

    @Getter
    private final Class<? extends CuisineRepository> jpaRepositoryClass = CuisineRepositoryImpl.class;

    @Getter
    @Autowired
    private CuisineRepository repository;


    @Test
    void addNotExistCuisines() {
        var cuisines = new ArrayList<Cuisine>();
        cuisines.add(cuisine());

        var names = cuisines.stream().map(Cuisine::getName).collect(Collectors.toSet());
        var found = repository.addNotExistCuisines(names);

        assertThat(found.stream().map(Cuisine::getName).collect(Collectors.toSet())).containsAll(names);
    }
}