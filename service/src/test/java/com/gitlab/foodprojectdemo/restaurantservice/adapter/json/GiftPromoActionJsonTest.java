package com.gitlab.foodprojectdemo.restaurantservice.adapter.json;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import com.gitlab.foodprojectdemo.restaurantservice.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.GiftPromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PercentDiscountPromoAction;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@JsonTest
class GiftPromoActionJsonTest extends AbstractTest {

    @Autowired
    private JacksonTester<GiftPromoAction> tester;

    @Test
    void serialize(GiftPromoAction promoAction) throws IOException {
        var json = tester.write(promoAction);
        assertThat(json).extractingJsonPathStringValue("$.type").isEqualTo("gift_promo_action");
        assertThat(json).extractingJsonPathNumberValue("$.id").isEqualTo(promoAction.getId());
        assertThat(json).extractingJsonPathNumberValue("$.restaurantId").isEqualTo(promoAction.getRestaurantId());
        assertThat(json).extractingJsonPathNumberValue("$.gifts[0].amount").isEqualTo(promoAction.getGifts().get(0).getAmount().longValue());
        assertThat(json).extractingJsonPathNumberValue("$.gifts[0].dishId").isEqualTo(promoAction.getGifts().get(0).getDishId());
        assertThat(json).extractingJsonPathNumberValue("$.gifts[1].amount").isEqualTo(promoAction.getGifts().get(1).getAmount().longValue());
        assertThat(json).extractingJsonPathNumberValue("$.gifts[1].dishId").isEqualTo(promoAction.getGifts().get(1).getDishId());
    }

    @Test
    void deserialize(GiftPromoAction promoAction) throws IOException {
        var json = "{\"type\":\"gift_promo_action\"," +
                "\"id\":\"" + promoAction.getId().toString() + "\"," +
                "\"restaurantId\":\"" + promoAction.getRestaurantId().toString() + "\"," +
                "\"discounts\": [{\"amount\": " + promoAction.getGifts().get(0).getAmount() + ",\"percent\": " + promoAction.getGifts().get(0).getDishId() + "}]}";

        log.info(json);

        var actual = tester.parse(json).getObject();
    }

}