package com.gitlab.foodprojectdemo.restaurantservice;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ActiveProfiles;
import com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions.AbsoluteDiscountPromoActionParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions.AbsoluteDiscountPromoCodeParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions.CategoryParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions.ChangeStatusCommandParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions.CuisineParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions.DishParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions.GiftPromoActionParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions.OrderIdParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions.PercentDiscountPromoActionParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions.PercentDiscountPromoCodeParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions.PromoActionParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions.RestaurantParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions.RestaurateurParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions.TicketParameterResolver;

@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
@ExtendWith({
        CuisineParameterResolver.class,
        PromoActionParameterResolver.class,
        AbsoluteDiscountPromoActionParameterResolver.class,
        AbsoluteDiscountPromoCodeParameterResolver.class,
        PercentDiscountPromoActionParameterResolver.class,
        PercentDiscountPromoCodeParameterResolver.class,
        CategoryParameterResolver.class,
        DishParameterResolver.class,
        GiftPromoActionParameterResolver.class,
        OrderIdParameterResolver.class,
        ChangeStatusCommandParameterResolver.class,
        RestaurantParameterResolver.class,
        TicketParameterResolver.class,
        RestaurateurParameterResolver.class
})
public abstract class AbstractTest {
}
