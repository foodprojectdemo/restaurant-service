package com.gitlab.foodprojectdemo.restaurantservice.fixture;

import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.Cuisine;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.LegalData;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.Restaurant;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantInfo;

import java.util.Collections;
import java.util.Set;

import static com.gitlab.foodprojectdemo.restaurantservice.Faker.faker;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.IdFixture.id;

public class RestaurantFixture {

    public static Long restaurantId() {
        return id();
    }

    public static Restaurant restaurant() {
        return restaurant(Collections.emptySet());
    }

    public static Restaurant restaurant(Set<Cuisine> cuisines) {
        return new Restaurant(
                restaurantId(),
                restaurantInfo(),
                cuisines
        );
    }

    public static RestaurantInfo restaurantInfo() {
        return RestaurantInfo.builder()
                .name(faker().name().title())
                .information(faker().lorem().paragraph())
                .logo(faker().internet().image())
                .openDays("1,2,3,4,5,6,7")
                .openFrom(String.format("%02d:00", faker().number().numberBetween(1, 12)))
                .openTo(String.format("%02d:00", faker().number().numberBetween(12, 24)))
                .legalData(legalData())
                .build();

    }

    public static LegalData legalData() {
        return LegalData.builder()
                .name(faker().name().title())
                .address(faker().address().fullAddress())
                .inn(faker().number().digits(10))
                .ogrn(faker().number().digits(13))
                .build();
    }
}
