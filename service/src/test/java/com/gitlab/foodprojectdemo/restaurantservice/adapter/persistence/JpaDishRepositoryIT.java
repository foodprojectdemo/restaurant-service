package com.gitlab.foodprojectdemo.restaurantservice.adapter.persistence;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.Dish;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.DishRepository;

import static org.assertj.core.api.Assertions.assertThat;

@RequiredArgsConstructor
class JpaDishRepositoryIT extends SimpleJpaDomainRepositoryIT<DishRepository, Dish, Long> {

    @Getter
    private final Dish aggregate;

    @Getter
    private final Class<? extends DishRepository> jpaRepositoryClass = JpaDishRepository.class;

    @Getter
    @Autowired
    private DishRepository repository;

    @Test
    void findByIdAndRestaurantId(Dish dish) {
        dish = repository.save(dish);

        //noinspection OptionalGetWithoutIsPresent
        assertThat(repository.findByIdAndRestaurantId(dish.getId(), dish.getRestaurantId()).get()).isEqualTo(dish);
    }
}