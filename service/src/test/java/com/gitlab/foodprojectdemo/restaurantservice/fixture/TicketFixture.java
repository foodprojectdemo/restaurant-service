package com.gitlab.foodprojectdemo.restaurantservice.fixture;

import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.Ticket;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.TicketItem;

import java.util.List;

import static com.gitlab.foodprojectdemo.restaurantservice.fixture.DishFixture.dish;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.OrderFixture.orderId;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.QuantityFixture.quantity;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.RestaurantFixture.restaurantId;

public class TicketFixture {

    public static Ticket newTicket() {
        return Ticket.create(orderId(), restaurantId(), List.of(ticketItem()));
    }

    public static Ticket acceptedTicket() {
        var ticket = newTicket();
        ticket.clearEvents();
        ticket.accept();
        return ticket;
    }

    public static Ticket rejectedTicket() {
        var ticket = newTicket();
        ticket.clearEvents();
        ticket.reject();
        return ticket;
    }

    public static Ticket readyTicket() {
        var ticket = acceptedTicket();
        ticket.clearEvents();
        ticket.ready();
        return ticket;
    }

    private static TicketItem ticketItem() {
        var dish = dish();
        return new TicketItem(dish.getId(), dish.getName(), quantity());
    }
}
