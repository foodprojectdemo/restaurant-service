package com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.gitlab.foodprojectdemo.restaurantservice.domain.Saved;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.Category;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.CategoryRepository;
import com.gitlab.foodprojectdemo.restaurantservice.fixture.CategoryFixture;

public class CategoryParameterResolver extends TypeBasedParameterResolver<Category> {

    @Override
    public Category resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        var category = CategoryFixture.category();
        if (parameterContext.isAnnotated(Saved.class)) {
            var applicationContext = SpringExtension.getApplicationContext(extensionContext);
            var categoryRepository = applicationContext.getBean(CategoryRepository.class);
            categoryRepository.save(category);
        }
        return category;
    }
}
