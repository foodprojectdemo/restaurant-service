package com.gitlab.foodprojectdemo.restaurantservice.contracts.self.messaging;

import org.junit.jupiter.api.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import com.gitlab.foodprojectdemo.restaurantservice.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantservice.adapter.messaging.DomainEventPublisher;
import com.gitlab.foodprojectdemo.restaurantservice.adapter.messaging.TestConsumer;
import com.gitlab.foodprojectdemo.restaurantservice.config.jackson.JacksonConfig;

import static com.gitlab.foodprojectdemo.restaurantservice.fixture.TicketFixture.acceptedTicket;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.TicketFixture.newTicket;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.TicketFixture.readyTicket;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.TicketFixture.rejectedTicket;


@SpringBootTest
@EmbeddedKafka(partitions = 1, topics = "${kafka.topic.events-topic}")
@AutoConfigureMessageVerifier
@ContextConfiguration(
        name = "kafka-configuration",
        classes = {
                JacksonConfig.class,
                KafkaAutoConfiguration.class,
                TestConsumer.class,
                DomainEventPublisher.class
        }
)
@EnableKafka
@Tag("kafka-test")
@TestPropertySource(properties = "spring.kafka.bootstrap-servers=${spring.embedded.kafka.brokers}")
public class EventsBaseTest extends AbstractTest {

    @Autowired
    private DomainEventPublisher domainEventPublisher;


    protected void ticketCreatedEvent() {
        domainEventPublisher.publish(newTicket().getEvents());
    }

    protected void ticketAcceptedEvent() {
        domainEventPublisher.publish(acceptedTicket().getEvents());
    }

    protected void ticketRejectedEvent() {
        domainEventPublisher.publish(rejectedTicket().getEvents());
    }

    protected void ticketReadyEvent() {
        domainEventPublisher.publish(readyTicket().getEvents());
    }
}
