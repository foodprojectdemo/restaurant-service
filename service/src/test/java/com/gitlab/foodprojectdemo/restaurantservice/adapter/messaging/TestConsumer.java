package com.gitlab.foodprojectdemo.restaurantservice.adapter.messaging;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.kafka.annotation.KafkaListener;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@TestComponent
public class TestConsumer {
    @Getter
    private final Map<String, ConsumerRecord<?, ?>> broker = new ConcurrentHashMap<>();

    @KafkaListener(
            groupId = "test-listener",
            topicPattern = ".*",
            clientIdPrefix = "${spring.kafka.client-id}-test"
    )
    void listener(ConsumerRecord<?, ?> record) {
        log.info("received {} form {}", record.value(), record.topic());
        broker.put(record.topic(), record);
    }
}
