package com.gitlab.foodprojectdemo.restaurantservice.system;

import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import com.gitlab.foodprojectdemo.restaurantservice.domain.Saved;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.Category;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.Dish;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.DishRepository;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.Restaurant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class DishesControllerIT extends AbstractSystemTest {

    @Autowired
    private DishRepository dishRepository;

    @Test
    void shouldCreateDish(@Saved Restaurant restaurant, @Saved Category category, Dish dish) throws Exception {
        var result = mockMvc.perform(
                post("/api/v1/restaurants/{restaurantId}/categories/{categoryId}/dishes", restaurant.getId(), category.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(dish.getInfo()))
        )
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn();

        var id = JsonPath.parse(result.getResponse().getContentAsString()).read("$.id", Long.class);

        assertThat(dishRepository.findById(id)).isPresent();
    }
}
