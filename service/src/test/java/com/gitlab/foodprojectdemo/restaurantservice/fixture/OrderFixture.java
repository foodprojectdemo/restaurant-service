package com.gitlab.foodprojectdemo.restaurantservice.fixture;

import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;

import java.util.UUID;

public class OrderFixture {
    public static OrderId orderId() {
        return new OrderId(UUID.randomUUID().toString());
    }

}
