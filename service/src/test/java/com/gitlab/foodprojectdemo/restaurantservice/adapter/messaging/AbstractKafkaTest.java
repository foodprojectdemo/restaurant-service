package com.gitlab.foodprojectdemo.restaurantservice.adapter.messaging;

import org.junit.jupiter.api.Tag;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import com.gitlab.foodprojectdemo.restaurantservice.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantservice.config.jackson.JacksonConfig;

@SpringBootTest
@ContextHierarchy(
        value = {
                @ContextConfiguration(
                        name = "kafka-configuration",
                        classes = {
                                JacksonConfig.class,
                                KafkaAutoConfiguration.class,
                                TestConsumer.class
                        }
                )
        }
)
@EnableKafka
@Tag("kafka-test")
public abstract class AbstractKafkaTest extends AbstractTest {
}
