package com.gitlab.foodprojectdemo.restaurantservice.fixture;

import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.AbsoluteDiscountPromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.DiscountItem;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.GiftItem;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.GiftPromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PercentDiscountItem;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PercentDiscountPromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoAction;

import java.util.List;
import java.util.Random;

import static com.gitlab.foodprojectdemo.restaurantservice.Faker.faker;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.IdFixture.id;

public class PromoActionFixture {

    public static PromoAction promoAction() {
        var list = List.of(
                absoluteDiscountPromoAction(),
                percentDiscountPromoAction()
        );
        return list.get(new Random().nextInt(list.size()));
    }

    public static AbsoluteDiscountPromoAction absoluteDiscountPromoAction() {
        return absoluteDiscountPromoAction(id());
    }

    public static AbsoluteDiscountPromoAction absoluteDiscountPromoAction(Long restaurantId) {
        return new AbsoluteDiscountPromoAction(
                id(),
                restaurantId,
                List.of(new DiscountItem(faker().number().numberBetween(500, 1000), faker().number().numberBetween(1, 200)),
                        new DiscountItem(faker().number().numberBetween(1000, 2000), faker().number().numberBetween(200, 400)))
        );
    }

    public static PercentDiscountPromoAction percentDiscountPromoAction() {
        return percentDiscountPromoAction(id());
    }

    public static PercentDiscountPromoAction percentDiscountPromoAction(Long restaurantId) {
        return new PercentDiscountPromoAction(
                id(),
                restaurantId,
                List.of(
                        new PercentDiscountItem(faker().number().numberBetween(1, 1000), faker().number().numberBetween(1, 49)),
                        new PercentDiscountItem(faker().number().numberBetween(1, 1000), faker().number().numberBetween(50, 99))
                )
        );
    }

    public static GiftPromoAction giftPromoAction() {
        return new GiftPromoAction(
                id(),
                id(),
                List.of(
                        new GiftItem(faker().number().numberBetween(1, 1000), faker().number().numberBetween(1, 49)),
                        new GiftItem(faker().number().numberBetween(1, 1000), faker().number().numberBetween(50, 99))
                )
        );
    }

}
