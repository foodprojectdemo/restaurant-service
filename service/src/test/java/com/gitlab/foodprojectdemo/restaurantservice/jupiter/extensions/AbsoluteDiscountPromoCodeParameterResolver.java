package com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.AbsoluteDiscountPromoCode;
import com.gitlab.foodprojectdemo.restaurantservice.fixture.PromoCodeFixture;

@Slf4j
public class AbsoluteDiscountPromoCodeParameterResolver extends TypeBasedParameterResolver<AbsoluteDiscountPromoCode> {

    @Override
    public AbsoluteDiscountPromoCode resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return PromoCodeFixture.absoluteDiscountPromoCode();
    }
}
