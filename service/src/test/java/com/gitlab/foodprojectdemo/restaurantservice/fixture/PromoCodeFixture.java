package com.gitlab.foodprojectdemo.restaurantservice.fixture;

import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.AbsoluteDiscountPromoCode;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PercentDiscountPromoCode;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoCode;

import java.util.List;
import java.util.Random;

import static com.gitlab.foodprojectdemo.restaurantservice.Faker.faker;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.IdFixture.id;

public class PromoCodeFixture {

    public static PromoCode promoCode() {
        var list = List.of(
                absoluteDiscountPromoCode(),
                percentDiscountPromoCode()
        );
        return list.get(new Random().nextInt(list.size()));
    }

    public static AbsoluteDiscountPromoCode absoluteDiscountPromoCode() {
        return absoluteDiscountPromoCode(id());
    }

    public static AbsoluteDiscountPromoCode absoluteDiscountPromoCode(Long restaurantId) {
        return new AbsoluteDiscountPromoCode(
                faker().lorem().word(),
                restaurantId,
                faker().number().numberBetween(1, 1000)
        );
    }

    public static PercentDiscountPromoCode percentDiscountPromoCode() {
        return percentDiscountPromoCode(id());
    }

    public static PercentDiscountPromoCode percentDiscountPromoCode(Long restaurantId) {
        return new PercentDiscountPromoCode(
                faker().lorem().word(),
                restaurantId,
                faker().number().numberBetween(1, 99)
        );
    }

}
