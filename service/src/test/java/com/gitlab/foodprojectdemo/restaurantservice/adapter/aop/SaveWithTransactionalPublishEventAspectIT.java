package com.gitlab.foodprojectdemo.restaurantservice.adapter.aop;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import com.gitlab.foodprojectdemo.restaurantservice.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantservice.adapter.messaging.DomainEventPublisher;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.Ticket;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.TicketRepository;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Import(SaveWithTransactionalPublishEventAspect.class)
@ComponentScan
@EnableAspectJAutoProxy
class SaveWithTransactionalPublishEventAspectIT extends AbstractTest {

    @Autowired
    private TicketRepository ticketRepository;

    @SpyBean
    private SaveWithTransactionalPublishEventAspect aspect;

    @MockBean
    private DomainEventPublisher domainEventPublisher;

    @Test
    void shouldSentEvents(Ticket ticket) {
        ticketRepository.save(ticket);

        verify(aspect).save(any());
        verify(domainEventPublisher).publish(ticket.getEvents());
    }
}