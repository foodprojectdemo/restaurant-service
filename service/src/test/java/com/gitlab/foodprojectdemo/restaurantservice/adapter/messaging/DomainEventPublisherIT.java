package com.gitlab.foodprojectdemo.restaurantservice.adapter.messaging;

import com.jayway.jsonpath.JsonPath;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainEvent;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RootAggregate;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(classes = {DomainEventPublisher.class})
class DomainEventPublisherIT extends AbstractKafkaTest {

    @Autowired
    private TestConsumer testConsumer;

    @Value("${kafka.topic.events-topic}")
    private String topic;

    @Autowired
    private DomainEventPublisher domainEventPublisher;


    @Getter
    @AllArgsConstructor
    private static class TestRootAggregate extends RootAggregate<TestRootAggregate, UUID> {
        UUID id;

        @Override
        public String getAggregateName() {
            return "test_root_aggregate";
        }
    }

    private static class TestDomainEvent extends DomainEvent<TestRootAggregate, UUID> {
        @Getter
        private final String type = "test_domain_event";

        public TestDomainEvent(@NonNull TestRootAggregate aggregate) {
            super(aggregate);
        }
    }


    @Test
    void shouldSendDomainEventsToTopic() {
        var testRootAggregate = new TestRootAggregate(UUID.randomUUID());
        var testDomainEvent = new TestDomainEvent(testRootAggregate);

        domainEventPublisher.publish(List.of(testDomainEvent));

        Awaitility.await().untilAsserted(() -> {
                    var consumerRecord = testConsumer.getBroker().get(topic);
                    assertThat(consumerRecord).isNotNull();
                    var json = JsonPath.parse(consumerRecord.value().toString());
                    assertThat(json.read("$.aggregateId", String.class)).isEqualTo(testDomainEvent.getAggregateId().toString());
                }
        );
    }

}