package com.gitlab.foodprojectdemo.restaurantservice.adapter.json;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import com.gitlab.foodprojectdemo.restaurantservice.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.AbsoluteDiscountPromoAction;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@JsonTest
class AbsoluteDiscountPromoActionJsonTest extends AbstractTest {

    @Autowired
    private JacksonTester<AbsoluteDiscountPromoAction> tester;

    @Test
    void serialize(AbsoluteDiscountPromoAction promoAction) throws IOException {
        var json = tester.write(promoAction);
        assertThat(json).extractingJsonPathStringValue("$.type").isEqualTo("absolute_promo_action");
        assertThat(json).extractingJsonPathNumberValue("$.id").isEqualTo(promoAction.getId());
        assertThat(json).extractingJsonPathNumberValue("$.restaurantId").isEqualTo(promoAction.getRestaurantId());
        assertThat(json).extractingJsonPathNumberValue("$.discounts[0].amount").isEqualTo(promoAction.getDiscounts().get(0).getAmount().longValue());
        assertThat(json).extractingJsonPathNumberValue("$.discounts[0].discount").isEqualTo(promoAction.getDiscounts().get(0).getDiscount().longValue());
        assertThat(json).extractingJsonPathNumberValue("$.discounts[1].amount").isEqualTo(promoAction.getDiscounts().get(1).getAmount().longValue());
        assertThat(json).extractingJsonPathNumberValue("$.discounts[1].discount").isEqualTo(promoAction.getDiscounts().get(1).getDiscount().longValue());
    }

    @Test
    void deserialize(AbsoluteDiscountPromoAction promoAction) throws IOException {
        var json = "{\"type\":\"absolute_promo_action\"," +
                "\"id\":\"" + promoAction.getId().toString() + "\"," +
                "\"restaurantId\":\"" + promoAction.getRestaurantId().toString() + "\"," +
                "\"discounts\": [{\"amount\": " + promoAction.getDiscounts().get(0).getAmount() + ",\"discount\": " + promoAction.getDiscounts().get(0).getDiscount() + "}]}";

        log.info(json);

        var actual = tester.parse(json).getObject();
    }

}