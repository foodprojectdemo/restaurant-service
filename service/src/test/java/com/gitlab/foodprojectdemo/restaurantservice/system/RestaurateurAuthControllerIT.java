package com.gitlab.foodprojectdemo.restaurantservice.system;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.PasswordEncoder;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.Credentials;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.Restaurateur;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.RestaurateurRepository;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


class RestaurateurAuthControllerIT extends AbstractSystemTest {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RestaurateurRepository restaurateurRepository;

    @Test
    void shouldSuccessfulAuth(Restaurateur restaurateur) throws Exception {
        var rawPassword = restaurateur.getPassword();
        restaurateur.setPassword(passwordEncoder.encode(rawPassword));
        restaurateur = restaurateurRepository.save(restaurateur);

        var credentials = new Credentials(restaurateur.getLogin(), rawPassword);

        mockMvc.perform(
                post("/api/v1/restaurateurs/auth")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(credentials))
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(restaurateur.getId()), Long.class));
    }

}