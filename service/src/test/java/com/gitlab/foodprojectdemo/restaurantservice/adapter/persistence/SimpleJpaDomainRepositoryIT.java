package com.gitlab.foodprojectdemo.restaurantservice.adapter.persistence;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import com.gitlab.foodprojectdemo.restaurantservice.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainRepository;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RootAggregate;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public abstract class SimpleJpaDomainRepositoryIT<R extends DomainRepository<A, ID>, A extends RootAggregate<A, ID>, ID> extends AbstractTest {

    @Autowired
    protected TestEntityManager entityManager;

    protected abstract R getRepository();

    protected abstract A getAggregate();

    protected abstract Class<? extends R> getJpaRepositoryClass();

    @Test
    void shouldBeJpaInstance() {
        assertThat(getRepository()).isInstanceOf(getJpaRepositoryClass());
    }

    @Test
    void shouldSaveAndFind() {
        var aggregate = getAggregate();
        shouldSaveAndFind(aggregate);
    }

    protected void shouldSaveAndFind(A aggregate) {
        var saved = getRepository().save(aggregate);
        saved.clearEvents();
        entityManager.flush();
        entityManager.clear();
        var actual = getRepository().findById(saved.getId());

        assertThat(actual.isPresent()).isTrue();
        assertThat(actual.get()).usingRecursiveComparison().isEqualTo(saved);
    }
}
