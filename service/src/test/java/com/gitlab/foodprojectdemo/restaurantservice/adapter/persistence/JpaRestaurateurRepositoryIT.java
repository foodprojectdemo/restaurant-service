package com.gitlab.foodprojectdemo.restaurantservice.adapter.persistence;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.Restaurateur;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.RestaurateurRepository;

@RequiredArgsConstructor
class JpaRestaurateurRepositoryIT extends SimpleJpaDomainRepositoryIT<RestaurateurRepository, Restaurateur, Long> {

    @Getter
    private final Restaurateur aggregate;

    @Getter
    private final Class<? extends RestaurateurRepository> jpaRepositoryClass = JpaRestaurateurRepository.class;

    @Getter
    @Autowired
    private RestaurateurRepository repository;

}