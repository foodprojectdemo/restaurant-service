package com.gitlab.foodprojectdemo.restaurantservice.adapter.persistence;

import lombok.Getter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.Restaurant;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantRepository;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class JpaRestaurantRepositoryIT extends SimpleJpaDomainRepositoryIT<RestaurantRepository, Restaurant, Long> {

    @Getter
    private final Restaurant aggregate;

    public JpaRestaurantRepositoryIT(Restaurant aggregate) {
        aggregate.setCuisines(Collections.emptySet());
        this.aggregate = aggregate;
    }

    @Getter
    private final Class<? extends RestaurantRepository> jpaRepositoryClass = JpaRestaurantRepository.class;

    @Getter
    @Autowired
    private RestaurantRepository repository;

    @Test
    void getAll(Restaurant restaurant1, Restaurant restaurant2) {
        restaurant1.setCuisines(Collections.emptySet());
        restaurant2.setCuisines(Collections.emptySet());
        var restaurants = List.of(restaurant1, restaurant2);
        var saved = ((JpaRestaurantRepository) repository).saveAll(restaurants);

        assertThat(repository.findAll()).containsAll(saved);
    }
}