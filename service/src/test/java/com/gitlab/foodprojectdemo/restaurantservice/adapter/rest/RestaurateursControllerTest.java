package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.Cuisine;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RegistrationService;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RegistrationService.RegistrationCommand;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.Restaurant;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantNameAlreadyExistsException;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.Credentials;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.Restaurateur;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.RestaurateurService;

import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@WebMvcTest(RestaurateursController.class)
class RestaurateursControllerTest extends AbstractRestTest {

    @MockBean
    private RegistrationService registrationService;

    @MockBean
    private RestaurateurService restaurateurService;

    @Nested
    class Registration {
        @Test
        void shouldCreateRestaurateur(Restaurateur restaurateur, Restaurant restaurant) throws Exception {
            var cuisines = cuisinesToTheirNames(restaurant.getCuisines());
            var command = new RegistrationCommand(restaurateur.getInfo(), restaurant.getInfo(), cuisines);

            when(registrationService.registration(command)).thenReturn(restaurateur);

            mockMvc.perform(
                    post("/api/v1/restaurateurs")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(command))
            )
                    .andDo(print())
                    .andExpect(status().isCreated())
                    .andExpect(jsonPath("$.id", is(restaurateur.getId()), Long.class))
                    .andDo(document("restaurateur-registration"));
        }

        @Test
        void shouldReturnNameAlreadyExistsError(Restaurateur restaurateur, Restaurant restaurant) throws Exception {
            var cuisines = cuisinesToTheirNames(restaurant.getCuisines());
            var command = new RegistrationCommand(restaurateur.getInfo(), restaurant.getInfo(), cuisines);

            when(registrationService.registration(command)).thenThrow(new RestaurantNameAlreadyExistsException(restaurant.getName()));

            mockMvc.perform(
                    post("/api/v1/restaurateurs")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(command))
            )
                    .andDo(print())
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.error", is("RESTAURANT_NAME_ALREADY_EXISTS")))
                    .andExpect(jsonPath("$.message", is(String.format("The name of the restaurant '%s' already exists", restaurant.getName()))));
        }

        private Set<String> cuisinesToTheirNames(Collection<Cuisine> cuisines) {
            return cuisines.stream().map(Cuisine::getName).collect(Collectors.toSet());
        }
    }

    @Nested
    class Authentication {
        @Test
        void shouldReturnRestaurateur(Restaurateur restaurateur) throws Exception {
            var credentials = new Credentials(restaurateur.getLogin(), restaurateur.getPassword());
            when(restaurateurService.auth(credentials)).thenReturn(Optional.of(restaurateur));

            var params = new HashMap<String, String>();
            params.put("login", restaurateur.getLogin());
            params.put("password", restaurateur.getPassword());

            mockMvc.perform(
                    post("/api/v1/restaurateurs/auth")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(params))
            )
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(restaurateur.getId()), Long.class))
                    .andExpect(jsonPath("$.restaurantId", is(restaurateur.getRestaurantId()), Long.class))
                    .andExpect(jsonPath("$.name", is(restaurateur.getName())))
                    .andExpect(jsonPath("$.login", is(restaurateur.getLogin())))
                    .andExpect(jsonPath("$.password").doesNotExist())
                    .andDo(document("restaurateur-authentication"));
        }

        @Test
        void shouldReturnInvalidLoginOPassword(Restaurateur restaurateur) throws Exception {
            var credentials = new Credentials(restaurateur.getLogin(), restaurateur.getPassword());
            when(restaurateurService.auth(credentials)).thenReturn(Optional.empty());

            mockMvc.perform(
                    post("/api/v1/restaurateurs/auth")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(credentials))
            )
                    .andDo(print())
                    .andExpect(status().isUnauthorized())
                    .andExpect(jsonPath("$.error", is("INVALID_LOGIN_OR_PASSWORD")))
                    .andExpect(jsonPath("$.message", is("Invalid login or password")))
                    .andDo(document("invalid-login-or-password-auth"));
        }
    }

}
