package com.gitlab.foodprojectdemo.restaurantservice.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import com.gitlab.foodprojectdemo.restaurantservice.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.Cuisine;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RegistrationService;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.Restaurant;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantService;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.Restaurateur;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.RestaurateurService;

import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class RegistrationServiceTest extends AbstractTest {
    @Mock
    private RestaurantService restaurantService;
    @Mock
    private RestaurateurService restaurateurService;

    private RegistrationService registrationService;

    @BeforeEach
    protected void setUp() {
        registrationService = new RegistrationService(restaurantService, restaurateurService);
    }

    @Test
    void shouldRegistration(Restaurateur restaurateur, Restaurant restaurant) {
        var cuisines = restaurant.getCuisines().stream().map(Cuisine::getName).collect(Collectors.toUnmodifiableSet());
        var command = new RegistrationService.RegistrationCommand(restaurateur.getInfo(), restaurant.getInfo(), cuisines);

        when(restaurantService.registration(command.restaurantRegistrationCommand())).thenReturn(restaurant);
        when(restaurateurService.registration(restaurant.getId(), command.getRestaurateur())).thenReturn(restaurateur);

        assertThat(registrationService.registration(command)).isEqualTo(restaurateur);
    }
}