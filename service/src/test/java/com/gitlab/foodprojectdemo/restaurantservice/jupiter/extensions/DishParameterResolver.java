package com.gitlab.foodprojectdemo.restaurantservice.jupiter.extensions;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.Dish;
import com.gitlab.foodprojectdemo.restaurantservice.fixture.DishFixture;

public class DishParameterResolver extends TypeBasedParameterResolver<Dish> {

    @Override
    public Dish resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return DishFixture.dish();
    }
}
