package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.AbsoluteDiscountPromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PercentDiscountPromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoActionService;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.Restaurant;

import java.nio.file.Files;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@WebMvcTest(PromoActionsController.class)
class PromoActionsControllerTest extends AbstractRestTest {

    @MockBean
    private PromoActionService promoActionService;

    @Value("classpath:payloads/create-promo-action-request.json")
    private Resource createPromoActionRequest;

    @Test
    void shouldReturnPromoActions(Restaurant restaurant, AbsoluteDiscountPromoAction absoluteDiscountPromoAction, PercentDiscountPromoAction percentDiscountPromoAction) throws Exception {
        var promoActions = List.of(absoluteDiscountPromoAction, percentDiscountPromoAction);
        when(promoActionService.promoActions(restaurant.getId())).thenAnswer(__ -> promoActions);

        mockMvc.perform(get("/api/v1/restaurants/{restaurantId}/promo-actions", restaurant.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(absoluteDiscountPromoAction.getId()), Long.class))
                .andExpect(jsonPath("$[0].restaurantId", is(absoluteDiscountPromoAction.getRestaurantId()), Long.class))
                .andExpect(jsonPath("$[0].type", is("absolute_promo_action")))
                .andExpect(jsonPath("$[0].discounts[0].amount", is(absoluteDiscountPromoAction.getDiscounts().get(0).getAmount())))
                .andExpect(jsonPath("$[0].discounts[0].discount", is(absoluteDiscountPromoAction.getDiscounts().get(0).getDiscount())))
                .andExpect(jsonPath("$[1].id", is(percentDiscountPromoAction.getId()), Long.class))
                .andExpect(jsonPath("$[1].restaurantId", is(percentDiscountPromoAction.getRestaurantId()), Long.class))
                .andExpect(jsonPath("$[1].type", is("percent_promo_action")))
                .andExpect(jsonPath("$[1].discounts[0].amount", is(percentDiscountPromoAction.getDiscounts().get(0).getAmount())))
                .andExpect(jsonPath("$[1].discounts[0].percent", is(percentDiscountPromoAction.getDiscounts().get(0).getPercent())))
                .andDo(document("get-promo-actions"));
    }

    @Test
    void shouldCreatePromoActions(PromoAction promoAction, Restaurant restaurant) throws Exception {
        when(promoActionService.create(any())).thenReturn(promoAction);

        mockMvc.perform(
                post("/api/v1/restaurants/{restaurantId}/promo-actions", restaurant.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Files.readString(createPromoActionRequest.getFile().toPath()))
        )
                .andDo(print())
                .andExpect(status().isCreated())
                .andDo(document("create-promo-action"));
    }
}