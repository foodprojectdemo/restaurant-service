package com.gitlab.foodprojectdemo.restaurantservice.contracts.self.rest;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.web.context.WebApplicationContext;
import com.gitlab.foodprojectdemo.restaurantservice.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantservice.adapter.rest.RestaurantsController;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantService;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.CuisineFixture.cuisine;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.RestaurantFixture.restaurant;

@WebMvcTest(RestaurantsController.class)
@AutoConfigureMessageVerifier
public class RestaurantsBaseTest extends AbstractTest {

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private RestaurantService restaurantService;

    @BeforeEach
    void setUp() {
        var restaurant = restaurant(Set.of(cuisine()));
        when(restaurantService.restaurants()).thenReturn(List.of(restaurant));
        when(restaurantService.restaurant(any())).thenReturn(Optional.of(restaurant));

        RestAssuredMockMvc.webAppContextSetup(context);
    }
}
