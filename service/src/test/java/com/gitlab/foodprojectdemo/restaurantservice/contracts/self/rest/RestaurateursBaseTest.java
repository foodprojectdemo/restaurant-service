package com.gitlab.foodprojectdemo.restaurantservice.contracts.self.rest;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.web.context.WebApplicationContext;
import com.gitlab.foodprojectdemo.restaurantservice.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantservice.adapter.rest.RestaurateursController;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RegistrationService;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.Credentials;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.Restaurateur;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.RestaurateurService;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest(RestaurateursController.class)
@AutoConfigureMessageVerifier
public class RestaurateursBaseTest extends AbstractTest {

    public static final String SUCCESSFUL_LOGIN = "successful_login@mail.com";
    public static final String SUCCESSFUL_PASSWORD = "password";

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private RestaurateurService restaurateurService;

    @MockBean
    private RegistrationService registrationService;


    @BeforeEach
    void setUp(Restaurateur restaurateur) {
        restaurateur.setLogin(SUCCESSFUL_LOGIN);
        restaurateur.setPassword(SUCCESSFUL_PASSWORD);
        var credentials = new Credentials(restaurateur.getLogin(), restaurateur.getPassword());
        when(restaurateurService.auth(credentials)).thenReturn(Optional.of(restaurateur));
        when(registrationService.registration(any())).thenReturn(restaurateur);

        RestAssuredMockMvc.webAppContextSetup(context);
    }
}
