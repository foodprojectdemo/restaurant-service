package com.gitlab.foodprojectdemo.restaurantservice.adapter.messaging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidTypeIdException;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.kafka.support.Acknowledgment;
import com.gitlab.foodprojectdemo.restaurantservice.AbstractTest;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainEventId;

import java.time.ZonedDateTime;
import java.util.UUID;
import java.util.function.Consumer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static com.gitlab.foodprojectdemo.restaurantservice.Faker.faker;

@Value
class TestDomainEventDto implements DomainEventDto<String> {
    DomainEventId id;
    String aggregate;
    String aggregateId;
    ZonedDateTime occurredOn;
}

@Slf4j
class EventListenerTest extends AbstractTest {

    @Mock
    private ObjectMapper objectMapper;

    @Mock
    private Consumer<TestDomainEventDto> subscriber;

    @Mock
    private Acknowledgment acknowledgment;

    @InjectMocks
    private EventListener eventListener;

    private final TestDomainEventDto testDomainEventDto = new TestDomainEventDto(
            new DomainEventId(),
            "test",
            UUID.randomUUID().toString(),
            ZonedDateTime.now()
    );


    @Test
    void shouldSendEventToSubscriber() throws JsonProcessingException {
        var json = faker().lorem().characters();

        when(objectMapper.readValue(eq(json), Mockito.<TypeReference<?>>any())).thenAnswer(__ -> testDomainEventDto);

        eventListener.subscribe(TestDomainEventDto.class, subscriber);

        eventListener.events(json, acknowledgment);


        verify(subscriber).accept(testDomainEventDto);
        verify(acknowledgment).acknowledge();
    }

    @Test
    void shouldSkipUnknownEvent() throws JsonProcessingException {
        var json = faker().lorem().characters();

        when(objectMapper.readValue(eq(json), Mockito.<TypeReference<?>>any())).thenThrow(InvalidTypeIdException.class);

        eventListener.subscribe(TestDomainEventDto.class, subscriber);

        eventListener.events(json, acknowledgment);

        verify(subscriber, never()).accept(any());
        verify(acknowledgment).acknowledge();
    }

}