package com.gitlab.foodprojectdemo.restaurantservice.fixture;

import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.Cuisine;

import static com.gitlab.foodprojectdemo.restaurantservice.Faker.faker;
import static com.gitlab.foodprojectdemo.restaurantservice.fixture.IdFixture.id;

public class CuisineFixture {
    public static Cuisine cuisine() {
        return new Cuisine(id(), faker().nation().nationality());
    }
}
