CREATE TABLE public.category
(
    id            bigserial    NOT NULL,
    restaurant_id bigint       NOT NULL,
    name          varchar(255) NOT NULL,
    version       bigint       NOT NULL
);

CREATE INDEX category_restaurant_id ON public.category USING btree (restaurant_id);