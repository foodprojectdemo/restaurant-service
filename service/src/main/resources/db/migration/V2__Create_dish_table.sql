CREATE TABLE public.dish
(
    id            bigserial    NOT NULL PRIMARY KEY,
    restaurant_id bigint       NOT NULL,
    category_id   bigint       NOT NULL,
    name          varchar(255) NOT NULL,
    description   text         NOT NULL,
    amount        integer      NOT NULL,
    image_url     varchar(255) NOT NULL,
    version       bigint       NOT NULL

);
CREATE INDEX dish_restaurant_id ON public.dish USING btree (restaurant_id);

CREATE INDEX dish_restaurant_id_category_id ON public.dish USING btree (restaurant_id, category_id);