ALTER TABLE public.restaurant
    ADD COLUMN information text         NOT NULL DEFAULT '',
    ADD COLUMN logo        varchar(255) NOT NULL DEFAULT '',
    ADD COLUMN open_days   varchar(13)  NOT NULL DEFAULT '',
    ADD COLUMN open_from   varchar(5)   NOT NULL DEFAULT '',
    ADD COLUMN open_to     varchar(5)   NOT NULL DEFAULT '',
    ADD COLUMN legal_name  varchar(255) NOT NULL DEFAULT '',
    ADD COLUMN address     varchar(255) NOT NULL DEFAULT '',
    ADD COLUMN inn         varchar(12)  NOT NULL DEFAULT '',
    ADD COLUMN ogrn        varchar(15)  NOT NULL DEFAULT '';