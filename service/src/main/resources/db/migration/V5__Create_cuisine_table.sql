CREATE TABLE public.cuisine
(
    id      bigserial    NOT NULL PRIMARY KEY,
    name    varchar(255) NOT NULL,
    version bigint       NOT NULL
);

CREATE UNIQUE INDEX cuisine_name ON public.cuisine USING btree (name);


CREATE TABLE public.restaurant_cuisines
(
    restaurant_id bigint NOT NULL,
    cuisine_id    bigint NOT NULL,

    CONSTRAINT fk_restaurant_id FOREIGN KEY (restaurant_id) REFERENCES public.restaurant,
    CONSTRAINT fk_cuisine_id FOREIGN KEY (cuisine_id) REFERENCES public.cuisine
);