CREATE TABLE public.restaurateur
(
    id            bigserial    NOT NULL PRIMARY KEY,
    restaurant_id bigint       NOT NULL,
    name          varchar(255) NOT NULL,
    login         varchar(255) NOT NULL,
    password      varchar(255) NOT NULL,
    version       bigint       NOT NULL
);

CREATE UNIQUE INDEX restaurateur_login ON public.restaurateur USING btree (login);
CREATE INDEX restaurateur_restaurant_id ON public.restaurateur USING btree (restaurant_id);