CREATE TYPE TICKET_STATUS AS ENUM (
    'NEW', 'ACCEPTED', 'REJECTED', 'READY'
    );

CREATE TABLE public.ticket
(
    id            varchar(255)  NOT NULL PRIMARY KEY,
    restaurant_id bigint        NOT NULL,
    status        TICKET_STATUS NOT NULL,
    version       bigint        NOT NULL
);

CREATE TABLE public.ticket_items
(
    ticket_id varchar(255) NOT NULL,
    dish_id   bigint       NOT NULL,
    name      varchar(255) NOT NULL,
    quantity  integer      NOT NULL,

    CONSTRAINT fk_ticket_id FOREIGN KEY (ticket_id) REFERENCES public.ticket
);