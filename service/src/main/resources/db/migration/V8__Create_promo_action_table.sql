CREATE TABLE public.promo_action
(
    id            bigserial   NOT NULL PRIMARY KEY,
    type          varchar(32) NOT NULL,
    restaurant_id bigint      NOT NULL,
    data          jsonb       NOT NULL,
    version       bigint      NOT NULL
);

CREATE INDEX promo_action_restaurant_id ON public.promo_action USING btree (restaurant_id);
CREATE INDEX promo_action_type ON public.promo_action USING btree (type);