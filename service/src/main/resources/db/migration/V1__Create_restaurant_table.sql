CREATE TABLE public.restaurant
(
    id      bigserial    NOT NULL PRIMARY KEY,
    name    varchar(255) NOT NULL unique,
    version bigint       NOT NULL
);
