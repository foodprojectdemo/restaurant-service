package com.gitlab.foodprojectdemo.restaurantservice.domain.misc;

import java.util.Optional;

public interface DomainRepository<A extends RootAggregate<A, ID>, ID> {
    Optional<A> findById(ID id);

    A save(A aggregate);
}
