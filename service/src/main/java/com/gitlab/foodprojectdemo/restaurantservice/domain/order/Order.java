package com.gitlab.foodprojectdemo.restaurantservice.domain.order;

import lombok.NonNull;
import lombok.Value;

import java.util.Collection;

@Value
public class Order {
    @NonNull OrderId id;
    @NonNull Long restaurantId;
    @NonNull String status;
    @NonNull Collection<OrderItem> items;
}