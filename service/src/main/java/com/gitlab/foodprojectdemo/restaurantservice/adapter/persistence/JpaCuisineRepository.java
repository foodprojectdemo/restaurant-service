package com.gitlab.foodprojectdemo.restaurantservice.adapter.persistence;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.Cuisine;

import java.util.Optional;
import java.util.Set;

public interface JpaCuisineRepository extends CrudRepository<Cuisine, Long> {

    Optional<Cuisine> findById(Long Id);

    @SuppressWarnings("unchecked")
    Cuisine save(Cuisine cuisine);

    @Modifying
    @Query(value = "insert into Cuisine(name, version) values (:name, 0) on conflict do nothing", nativeQuery = true)
    void insertNotExistCuisine(String name);

    @Query("select c from Cuisine c WHERE c.name in (:names)")
    Set<Cuisine> findAllByNames(Set<String> names);
}
