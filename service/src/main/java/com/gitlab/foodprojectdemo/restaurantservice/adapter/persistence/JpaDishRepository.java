package com.gitlab.foodprojectdemo.restaurantservice.adapter.persistence;

import org.springframework.data.repository.CrudRepository;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.Dish;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.DishRepository;

import java.util.Optional;

public interface JpaDishRepository extends CrudRepository<Dish, Long>, DishRepository {

    @Override
    Optional<Dish> findById(Long Id);

    @SuppressWarnings("unchecked")
    @Override
    Dish save(Dish dish);
}
