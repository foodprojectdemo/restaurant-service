package com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant;

import com.gitlab.foodprojectdemo.restaurantservice.domain.exception.NotFoundDomainException;

public class RestaurantNotFoundException extends NotFoundDomainException {

    public RestaurantNotFoundException(Long restaurantId) {
        super(String.format("Restaurant with ID %d was not found", restaurantId));
    }
}
