package com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur;

import com.gitlab.foodprojectdemo.restaurantservice.domain.exception.DomainException;

public class LoginAlreadyExistsException extends DomainException {

    public LoginAlreadyExistsException(String login) {
        super(String.format("Login %s already exists", login));
    }
}
