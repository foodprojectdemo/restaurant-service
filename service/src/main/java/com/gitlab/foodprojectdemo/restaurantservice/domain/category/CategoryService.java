package com.gitlab.foodprojectdemo.restaurantservice.domain.category;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RelationCheckService;

import java.util.Collection;

@Service
@AllArgsConstructor
public class CategoryService {
    private final RelationCheckService relationCheckService;
    private final CategoryRepository categoryRepository;

    @Transactional
    public Category addCategory(Long restaurantId, CategoryInfo info) {
        var category = new Category(restaurantId, info);
        relationCheckService.checkRestaurantExists(category.getRestaurantId());
        return categoryRepository.save(category);
    }

    @Transactional(readOnly = true)
    public Collection<Category> categories(Long restaurantId) {
        relationCheckService.checkRestaurantExists(restaurantId);
        return categoryRepository.findByRestaurantId(restaurantId);
    }

}
