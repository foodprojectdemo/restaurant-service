package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class ErrorResponse {
    String message;
    String error;

    public ErrorResponse(Throwable throwable, String error) {
        this(throwable.getMessage(), error);
    }
}
