package com.gitlab.foodprojectdemo.restaurantservice.domain.dish;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Delegate;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RootAggregate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "dish", indexes = @Index(name = "restaurantId", columnList = "restaurantId"))
@JsonPropertyOrder({"id", "restaurantId", "categoryId", "name", "description", "amount", "imageUrl"})
public class Dish extends RootAggregate<Dish, Long> {
    @Transient
    private final String aggregateName = "dish";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    private Long restaurantId;

    @NonNull
    private Long categoryId;

    @NonNull
    @JsonIgnore
    @Delegate
    private DishInfo info;
}
