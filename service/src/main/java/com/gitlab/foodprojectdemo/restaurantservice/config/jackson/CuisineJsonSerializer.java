package com.gitlab.foodprojectdemo.restaurantservice.config.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.boot.jackson.JsonComponent;
import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.Cuisine;

import java.io.IOException;

@JsonComponent
class CuisineJsonSerializer extends JsonSerializer<Cuisine> {
    @Override
    public void serialize(Cuisine cuisine, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(cuisine.getName());
    }
}
