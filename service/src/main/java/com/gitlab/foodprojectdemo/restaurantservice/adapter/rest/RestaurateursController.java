package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RegistrationService;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RegistrationService.RegistrationCommand;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.Credentials;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.Restaurateur;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.RestaurateurService;

import javax.validation.Valid;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/v1/restaurateurs")
public class RestaurateursController {
    private final RegistrationService registrationService;
    private final RestaurateurService restaurateurService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public IdResponse<?> registration(@Valid @RequestBody RegistrationCommand command) {
        var restaurateur = registrationService.registration(command);
        return new IdResponse<>(restaurateur.getId());
    }


    @PostMapping("/auth")
    public Restaurateur auth(@Valid @RequestBody Credentials credentials) {
        return restaurateurService.auth(credentials)
                .orElseThrow(InvalidLoginOrPasswordException::new);
    }

}
