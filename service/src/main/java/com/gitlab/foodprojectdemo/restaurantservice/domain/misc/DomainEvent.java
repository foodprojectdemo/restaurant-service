package com.gitlab.foodprojectdemo.restaurantservice.domain.misc;

import lombok.NonNull;
import lombok.Value;
import lombok.experimental.NonFinal;

import java.time.ZonedDateTime;

@Value
@NonFinal
public abstract class DomainEvent<A extends RootAggregate<A, ID>, ID> {
    @NonNull DomainEventId id;
    @NonNull String aggregate;
    @NonNull ID aggregateId;
    @NonNull ZonedDateTime occurredOn;
    Object body;

    public DomainEvent(@NonNull A aggregate) {
        this(aggregate, null);
    }

    public DomainEvent(@NonNull A aggregate, Object body) {
        this.id = new DomainEventId();
        this.aggregate = aggregate.getAggregateName();
        this.aggregateId = aggregate.getId();
        this.occurredOn = ZonedDateTime.now();
        this.body = body;
    }

    public abstract String getType();
}
