package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;

import lombok.Value;

@Value
public class IdResponse<ID> {
    ID id;
}
