package com.gitlab.foodprojectdemo.restaurantservice.domain.ticket;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;

import java.util.List;


@Slf4j
@Service
@AllArgsConstructor
public class TicketService {
    private final TicketRepository ticketRepository;

    @Transactional(readOnly = true)
    public List<Ticket> tickets(Long restaurantId) {
        return ticketRepository.findByRestaurantId(restaurantId);
    }

    @Transactional
    public Ticket create(CreateTicketCommand command) {
        return ticketRepository.save(command.createTicket());
    }

    @Transactional
    public Ticket changeStatus(ChangeStatusCommand command) {
        var ticket = ticketOrException(command.getOrderId(), command.getRestaurantId());
        command.changeStatus(ticket);
        return ticketRepository.save(ticket);
    }

    private Ticket ticketOrException(OrderId orderId, Long restaurantId) {
        return ticketRepository.findByIdAndRestaurantId(orderId, restaurantId).orElseThrow(() -> new TicketNotFoundException(restaurantId, orderId));
    }

}
