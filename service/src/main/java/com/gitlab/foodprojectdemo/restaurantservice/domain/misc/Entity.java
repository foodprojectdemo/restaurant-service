package com.gitlab.foodprojectdemo.restaurantservice.domain.misc;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

@MappedSuperclass
public abstract class Entity<ID> {

    @Version
    @JsonIgnore
    private Long version;

    public abstract ID getId();

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!getClass().equals(obj.getClass())) return false;

        var anotherId = getClass().cast(obj).getId();
        return getId().equals(anotherId);
    }
}
