package com.gitlab.foodprojectdemo.restaurantservice.domain.promo;

import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainRepository;

import java.util.Collection;

public interface PromoActionRepository extends DomainRepository<PromoAction, Long> {
    Collection<PromoAction> findByRestaurantId(Long restaurantId);
    Collection<PromoAction> findAll();
}
