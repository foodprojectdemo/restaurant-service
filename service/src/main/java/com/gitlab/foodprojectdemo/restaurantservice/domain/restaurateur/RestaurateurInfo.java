package com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.With;

import javax.persistence.Embeddable;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Embeddable
public class RestaurateurInfo {
    @NonNull
    private String name;

    @NonNull
    private String login;

    @NonNull
    @With
    private String password;
}
