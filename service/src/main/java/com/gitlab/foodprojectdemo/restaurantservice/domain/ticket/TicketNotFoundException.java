package com.gitlab.foodprojectdemo.restaurantservice.domain.ticket;

import com.gitlab.foodprojectdemo.restaurantservice.domain.exception.NotFoundDomainException;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;

public class TicketNotFoundException extends NotFoundDomainException {

    public TicketNotFoundException(Long restaurantId, OrderId orderId) {
        super(String.format("Ticket %s %s not found", restaurantId, orderId));
    }
}
