package com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RootAggregate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cuisine", indexes = @Index(name = "cuisine_name", columnList = "name"))
public class Cuisine extends RootAggregate<Cuisine, Long> {
    @Transient
    private final String aggregateName = "cuisine";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Column(updatable = false)
    private String name;
}
