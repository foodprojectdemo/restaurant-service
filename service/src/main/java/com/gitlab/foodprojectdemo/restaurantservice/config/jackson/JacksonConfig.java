package com.gitlab.foodprojectdemo.restaurantservice.config.jackson;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.boot.jackson.JsonComponent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.Cuisine;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainEventId;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.NewStatus;

@JsonComponent
@Configuration
public class JacksonConfig {

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .enable(DeserializationFeature.USE_LONG_FOR_INTS)
                .registerModule(new JavaTimeModule())
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .registerModule(
                        new SimpleModule(
                                "JSONModule",
                                new Version(2, 0, 0, null, null, null)
                        )
                                .addSerializer(Cuisine.class, new CuisineJsonSerializer())
                                .addSerializer(OrderId.class, new ToStringSerializer())
                                .addDeserializer(OrderId.class, new OrderIdJsonDeserializer())
                                .addDeserializer(NewStatus.class, new NewStatusJsonDeserializer())
                                .addSerializer(DomainEventId.class, new DomainEventIdJsonSerializer())
                                .addDeserializer(DomainEventId.class, new DomainEventIdJsonDeserializer())
                );
    }
}
