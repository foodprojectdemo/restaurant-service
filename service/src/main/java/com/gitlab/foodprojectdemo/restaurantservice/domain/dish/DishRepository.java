package com.gitlab.foodprojectdemo.restaurantservice.domain.dish;

import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainRepository;

import java.util.Collection;
import java.util.Optional;

public interface DishRepository extends DomainRepository<Dish, Long> {

    Optional<Dish> findByIdAndRestaurantId(Long id, Long restaurantId);

    Collection<Dish> findByRestaurantId(Long restaurantId);

}
