package com.gitlab.foodprojectdemo.restaurantservice.config.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.boot.jackson.JsonComponent;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainEventId;

import java.io.IOException;
import java.util.UUID;

@JsonComponent
class DomainEventIdJsonDeserializer extends JsonDeserializer<DomainEventId> {

    @Override
    public DomainEventId deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        return new DomainEventId(UUID.fromString(node.textValue()));
    }
}
