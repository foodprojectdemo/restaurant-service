package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;


import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.Category;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.CategoryInfo;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.CategoryService;

import javax.validation.Valid;
import java.util.Collection;

@RequestMapping(value = "/api/v1/restaurants/{restaurantId}/categories")
@RestController
@AllArgsConstructor
public class CategoriesController {
    private final CategoryService categoryService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public IdResponse<?> addCategory(@PathVariable Long restaurantId, @Valid @RequestBody CategoryInfo info) {
        var category = categoryService.addCategory(restaurantId, info);
        return new IdResponse<>(category.getId());
    }

    @GetMapping
    public Collection<Category> categories(@PathVariable Long restaurantId) {
        return categoryService.categories(restaurantId);
    }
}
