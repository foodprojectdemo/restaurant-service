package com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.PasswordEncoder;

import java.util.Optional;

@Service
@AllArgsConstructor
public class RestaurateurService {
    private final RestaurateurRepository restaurateurRepository;
    private final PasswordEncoder passwordEncoder;

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Restaurateur registration(Long restaurantId, RestaurateurInfo info) {
        restaurateurRepository.findByLogin(info.getLogin()).ifPresent(restaurateur -> {
            throw new LoginAlreadyExistsException(restaurateur.getLogin());
        });

        var infoWithEncryptedPassword = info.withPassword(passwordEncoder.encode(info.getPassword()));
        var restaurateur = new Restaurateur(restaurantId, infoWithEncryptedPassword);
        return restaurateurRepository.save(restaurateur);
    }

    @Transactional(readOnly = true)
    public Optional<Restaurateur> auth(Credentials credentials) {
        var restaurateur = restaurateurRepository.findByLogin(credentials.getLogin());

        if (restaurateur.isPresent() && passwordEncoder.matches(credentials.getPassword(), restaurateur.get().getPassword())) {
            return restaurateur;
        }
        return Optional.empty();
    }
}
