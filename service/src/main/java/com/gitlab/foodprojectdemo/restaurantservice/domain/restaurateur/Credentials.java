package com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur;

import lombok.Value;

import javax.validation.constraints.NotEmpty;

@Value
public class Credentials {
    @NotEmpty
    String login;
    @NotEmpty
    String password;
}
