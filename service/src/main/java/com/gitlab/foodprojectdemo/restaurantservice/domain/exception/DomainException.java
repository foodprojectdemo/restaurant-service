package com.gitlab.foodprojectdemo.restaurantservice.domain.exception;

public class DomainException extends RuntimeException {

    public DomainException(String message) {
        super(message);
    }
}
