package com.gitlab.foodprojectdemo.restaurantservice.domain.promo;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@AllArgsConstructor
public class PromoActionService {
    private final PromoActionRepository promoActionRepository;

    public Collection<PromoAction> promoActions(Long restaurantId) {
        return promoActionRepository.findByRestaurantId(restaurantId);
    }

    public PromoAction create(PromoAction promoAction) {
        return promoActionRepository.save(promoAction);
    }

}
