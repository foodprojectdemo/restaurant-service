package com.gitlab.foodprojectdemo.restaurantservice.domain.misc;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Transient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class RootAggregate<A extends RootAggregate<A, ID>, ID> extends Entity<ID> {

    @Transient
    @JsonIgnore
    private final List<DomainEvent<A, ID>> events = new ArrayList<>();

    @JsonIgnore
    public abstract String getAggregateName();

    public List<DomainEvent<A, ID>> getEvents() {
        return Collections.unmodifiableList(events);
    }

    public void clearEvents() {
        events.clear();
    }

    protected void publishEvent(DomainEvent<A, ID> domainEvent) {
        events.add(domainEvent);
    }

}
