package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoCode;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoCodeNotFoundException;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoCodeService;

import java.util.Collection;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/restaurants/{restaurantId}/promo-codes")
public class PromoCodesController {
    private final PromoCodeService promoCodeService;

    @GetMapping
    public Collection<PromoCode> promoCodes(@PathVariable Long restaurantId) {
        return promoCodeService.promoCodes(restaurantId);
    }

    @GetMapping("/{code}")
    public PromoCode promoCode(@PathVariable Long restaurantId, @PathVariable String code) {
        return promoCodeService.promoCode(restaurantId, code).orElseThrow(() -> new PromoCodeNotFoundException(restaurantId, code));
    }
}
