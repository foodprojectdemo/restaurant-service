package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import com.gitlab.foodprojectdemo.restaurantservice.config.jackson.IllegalValueObjectException;
import com.gitlab.foodprojectdemo.restaurantservice.domain.exception.NotFoundDomainException;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantNameAlreadyExistsException;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.LoginAlreadyExistsException;

import java.util.Objects;

@Slf4j
@RestControllerAdvice
class ControllerExceptionHandler {

    @ExceptionHandler(InvalidLoginOrPasswordException.class)
    private ResponseEntity<ErrorResponse> exceptionHandler() {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body(new ErrorResponse("Invalid login or password", "INVALID_LOGIN_OR_PASSWORD"));
    }

    @ExceptionHandler({LoginAlreadyExistsException.class, RestaurantNameAlreadyExistsException.class})
    private ResponseEntity<ErrorResponse> badRequestExceptionHandler(RuntimeException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ErrorResponse(exception,"RESTAURANT_NAME_ALREADY_EXISTS"));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    private ResponseEntity<ErrorResponse> exceptionHandler(MethodArgumentNotValidException exception) {
        var field = Objects.requireNonNull(exception.getBindingResult().getFieldError()).getField();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ErrorResponse(String.format("%s has invalid value %s", field, exception), "INVALID_VALUE"));
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    private ResponseEntity<ErrorResponse> exceptionHandler(HttpMessageNotReadableException exception) {
        Throwable cause = null;
        try {
            cause = exception.getCause().getCause();
        } catch (NullPointerException ignored) {

        }
        if (cause instanceof IllegalValueObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ErrorResponse(cause.getMessage(), "INVALID_VALUE"));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ErrorResponse("Required request body is missing" + exception.getCause(), "REQUIRED_REQUEST_BODY_IS_MISSING"));
    }


    @ExceptionHandler(NotFoundDomainException.class)
    private ResponseEntity<ErrorResponse> exceptionHandler(NotFoundDomainException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ErrorResponse(exception.getMessage(), HttpStatus.NOT_FOUND.name()));
    }

    @ExceptionHandler(RuntimeException.class)
    private ResponseEntity<ErrorResponse> exceptionHandler(RuntimeException exception) {
        log.error(exception.getMessage(), exception);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ErrorResponse("Please try later", HttpStatus.INTERNAL_SERVER_ERROR.name()));
    }
}
