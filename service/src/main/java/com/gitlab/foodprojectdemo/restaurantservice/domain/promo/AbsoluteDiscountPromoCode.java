package com.gitlab.foodprojectdemo.restaurantservice.domain.promo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class AbsoluteDiscountPromoCode extends PromoCode {
    int discount;

    public AbsoluteDiscountPromoCode(String code, Long restaurantId, int discount) {
        super(code, restaurantId);
        assert discount > 0 : "discount must be greater than 0";
        this.discount = discount;
    }
}
