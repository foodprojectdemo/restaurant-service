package com.gitlab.foodprojectdemo.restaurantservice.domain.misc;

public interface PasswordEncoder {
    String encode(CharSequence rawPassword);

    boolean matches(CharSequence rawPassword, String encodedPassword);
}
