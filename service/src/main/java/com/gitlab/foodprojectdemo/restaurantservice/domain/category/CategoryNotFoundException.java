package com.gitlab.foodprojectdemo.restaurantservice.domain.category;

import com.gitlab.foodprojectdemo.restaurantservice.domain.exception.NotFoundDomainException;

public class CategoryNotFoundException extends NotFoundDomainException {

    public CategoryNotFoundException(Long categoryId) {
        super(String.format("Category with ID %d was not found", categoryId));
    }
}
