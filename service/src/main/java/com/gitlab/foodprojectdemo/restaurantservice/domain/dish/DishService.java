package com.gitlab.foodprojectdemo.restaurantservice.domain.dish;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RelationCheckService;

import java.util.Collection;
import java.util.Optional;

@Service
@AllArgsConstructor
public class DishService {
    private final RelationCheckService relationCheckService;
    private final DishRepository dishRepository;

    @Transactional
    public Dish addDish(Long restaurantId, Long categoryId, DishInfo info) {
        var dish = new Dish(restaurantId, categoryId, info);
        relationCheckService.checkRestaurantExists(dish.getRestaurantId());
        relationCheckService.checkCategoryExists(dish.getCategoryId());

        return dishRepository.save(dish);
    }

    @Transactional(readOnly = true)
    public Collection<Dish> dishes(Long restaurantId) {
        relationCheckService.checkRestaurantExists(restaurantId);
        return dishRepository.findByRestaurantId(restaurantId);
    }

    @Transactional(readOnly = true)
    public Optional<Dish> dish(Long dishId) {
        return dishRepository.findById(dishId);
    }
}
