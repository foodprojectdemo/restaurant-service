package com.gitlab.foodprojectdemo.restaurantservice.adapter.persistence;

import org.springframework.data.repository.CrudRepository;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.Category;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.CategoryRepository;

public interface JpaCategoryRepository extends CrudRepository<Category, Long>, CategoryRepository {

}
