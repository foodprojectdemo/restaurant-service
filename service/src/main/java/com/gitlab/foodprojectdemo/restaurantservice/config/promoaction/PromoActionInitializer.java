package com.gitlab.foodprojectdemo.restaurantservice.config.promoaction;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoActionRepository;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantRepository;

import javax.annotation.PostConstruct;

@Slf4j
@Configuration
@AllArgsConstructor
class PromoActionInitializer {
    private final RestaurantRepository restaurantRepository;
    private final PromoActionRepository promoActionRepository;
    private final PromoActionGenerator promoActionGenerator;

    @PostConstruct
    void init() {
        if (promoActionRepository.findAll().isEmpty()) {
            log.info("Initialize Promo Actions...");
            restaurantRepository.findAll()
                    .stream()
                    .flatMap(restaurant -> promoActionGenerator.promoActionsGenerator(restaurant).stream())
                    .forEach(promoActionRepository::save);
        }
    }
}
