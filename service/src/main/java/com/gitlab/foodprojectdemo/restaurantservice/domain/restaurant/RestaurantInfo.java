package com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Getter
@Setter(AccessLevel.PROTECTED)
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Embeddable
public class RestaurantInfo {

    @NonNull
    private String name;

    @NonNull
    private String information;

    @NonNull
    private String logo;

    @NonNull
    private String openDays;
    @NonNull
    private String openFrom;
    @NonNull
    private String openTo;

    @NonNull
    @AttributeOverrides({
            @AttributeOverride(name = "name", column = @Column(name = "legal_name"))
    })
    private LegalData legalData;
}
