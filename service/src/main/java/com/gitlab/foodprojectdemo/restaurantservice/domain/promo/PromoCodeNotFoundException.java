package com.gitlab.foodprojectdemo.restaurantservice.domain.promo;

import com.gitlab.foodprojectdemo.restaurantservice.domain.exception.NotFoundDomainException;

public class PromoCodeNotFoundException extends NotFoundDomainException {

    public PromoCodeNotFoundException(Long restaurantId, String code) {
        super(String.format("Promo Code [%s] in restaurant %d was not found", code, restaurantId));
    }
}
