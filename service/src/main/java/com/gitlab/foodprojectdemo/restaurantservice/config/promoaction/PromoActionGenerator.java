package com.gitlab.foodprojectdemo.restaurantservice.config.promoaction;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.DishRepository;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.AbsoluteDiscountPromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.DiscountItem;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.GiftItem;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.GiftPromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PercentDiscountItem;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PercentDiscountPromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.Restaurant;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

@Service
@AllArgsConstructor
class PromoActionGenerator {
    private static final int MAX_PERCENT = 50;
    private final Random random = new Random();
    private final DishRepository dishRepository;

    public Collection<PromoAction> promoActionsGenerator(Restaurant restaurant) {
        if (dishRepository.findByRestaurantId(restaurant.getId()).isEmpty()) {
            return List.of(
                    new AbsolutePromoActionGenerator(restaurant).promoAction(),
                    new PercentPromoActionGenerator(restaurant).promoAction()
            );
        } else {
            return List.of(
                    new AbsolutePromoActionGenerator(restaurant).promoAction(),
                    new PercentPromoActionGenerator(restaurant).promoAction(),
                    new GiftPromoActionGenerator(restaurant, dishRepository).promoAction()
            );
        }
    }

    @RequiredArgsConstructor
    private abstract class Template<P extends PromoAction, I> {
        final int maxItems = random(2, 5);
        final int maxAmount = randomAmount(3000, 5000);
        final Restaurant restaurant;

        protected abstract P promoAction();

        Collection<I> items() {
            var items = new ArrayList<I>(maxItems);
            for (int i = 0; i < maxItems; i++) {
                if (i > 0) {
                    var previous = items.get(items.size() - 1);
                    items.add(item(previous));
                } else {
                    items.add(item(null));
                }
            }

            return items;
        }

        protected abstract I item(@Nullable I previous);
    }

    private class AbsolutePromoActionGenerator extends Template<AbsoluteDiscountPromoAction, DiscountItem> {

        public AbsolutePromoActionGenerator(Restaurant restaurant) {
            super(restaurant);
        }

        @Override
        protected AbsoluteDiscountPromoAction promoAction() {
            return new AbsoluteDiscountPromoAction(null, restaurant.getId(), items());
        }

        @Override
        protected DiscountItem item(@Nullable DiscountItem previous) {
            var amount = randomAmount(maxAmount / maxItems / 5, maxAmount / maxItems);
            var percent = randomPercent(MAX_PERCENT / maxItems / 2, MAX_PERCENT / maxItems);
            var discount = (int) (percent * amount) / 10 * 10;

            if (previous != null) {
                return new DiscountItem(previous.getAmount() + amount, previous.getDiscount() + discount);
            }
            return new DiscountItem(amount, discount);
        }
    }

    private class PercentPromoActionGenerator extends Template<PercentDiscountPromoAction, PercentDiscountItem> {

        public PercentPromoActionGenerator(Restaurant restaurant) {
            super(restaurant);
        }

        @Override
        protected PercentDiscountPromoAction promoAction() {
            return new PercentDiscountPromoAction(null, restaurant.getId(), items());
        }

        @Override
        protected PercentDiscountItem item(@Nullable PercentDiscountItem previous) {
            var amount = randomAmount(maxAmount / maxItems / 5, maxAmount / maxItems);
            var percent = (int) (randomPercent(MAX_PERCENT / maxItems / 2, MAX_PERCENT / maxItems) * 100);

            if (previous != null) {
                return new PercentDiscountItem(previous.getAmount() + amount, previous.getPercent() + percent);
            }
            return new PercentDiscountItem(amount, percent);
        }
    }

    private class GiftPromoActionGenerator extends Template<GiftPromoAction, GiftItem> {
        private final DishRepository dishRepository;

        public GiftPromoActionGenerator(Restaurant restaurant, DishRepository dishRepository) {
            super(restaurant);
            this.dishRepository = dishRepository;
        }

        @Override
        protected GiftPromoAction promoAction() {
            return new GiftPromoAction(null, restaurant.getId(), items());
        }

        @Override
        protected GiftItem item(GiftItem previous) {
            var amount = randomAmount(maxAmount / maxItems / 5, maxAmount / maxItems);
            var dishes = dishRepository.findByRestaurantId(restaurant.getId());
            if (!dishes.isEmpty()) {
                var dishId = dishes.stream().skip(random.nextInt(dishes.size())).findFirst().get().getId();

                if (previous != null) {
                    return new GiftItem(previous.getAmount() + amount, dishId);
                }
                return new GiftItem(amount, dishId);
            } else {
                throw new IllegalStateException();
            }
        }
    }

    private int random(int min, int max) {
        assert max > min : "Max must be greater than min";
        return min + random.nextInt(max - min + 1);
    }

    private double randomPercent(int min, int max) {
        assert max > min : "Max must be greater than min";
        return (min + random.nextInt(max - min)) * 0.01;
    }

    private int randomAmount(int min, int max) {
        assert max > min : "Max must be greater than min";
        return (min + random.nextInt(max - min + 1)) / 100 * 100;
    }
}
