package com.gitlab.foodprojectdemo.restaurantservice.adapter.gateway.order;

import lombok.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@Value
@ConstructorBinding
@ConfigurationProperties("order-service")
class OrderGatewayProperties {
  String url;
  String apiBaseUri;
  String orderUri;

  public String getBaseUri() {
    return url + apiBaseUri;
  }
}
