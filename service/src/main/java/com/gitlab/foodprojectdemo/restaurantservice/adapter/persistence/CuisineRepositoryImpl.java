package com.gitlab.foodprojectdemo.restaurantservice.adapter.persistence;

import lombok.AllArgsConstructor;
import lombok.experimental.Delegate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.Cuisine;
import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.CuisineRepository;

import java.util.Set;

@Slf4j
@Repository
@AllArgsConstructor
public class CuisineRepositoryImpl implements CuisineRepository {

    @Delegate
    private final JpaCuisineRepository jpaCuisineRepository;

    @Override
    @Transactional
    public Set<Cuisine> addNotExistCuisines(Set<String> names) {
        names.forEach(jpaCuisineRepository::insertNotExistCuisine);
        return jpaCuisineRepository.findAllByNames(names);
    }

}
