package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.ChangeStatusCommand;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.NewStatus;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.Ticket;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.TicketService;

import java.util.Collection;

@Value
class StatusDto {
   @NonNull NewStatus status;
}

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/restaurants/{restaurantId}/tickets")
public class TicketsController {
    private final TicketService ticketService;

    @GetMapping
    public Collection<Ticket> tickets(@PathVariable Long restaurantId) {
        return ticketService.tickets(restaurantId);
    }

    @PatchMapping("/{orderId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void changeStatus(@PathVariable Long restaurantId, @PathVariable OrderId orderId, @RequestBody StatusDto statusDto) {
        ticketService.changeStatus(new ChangeStatusCommand(orderId, restaurantId, statusDto.getStatus()));
    }
}
