package com.gitlab.foodprojectdemo.restaurantservice.domain.category;

import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainRepository;

import java.util.Collection;

public interface CategoryRepository extends DomainRepository<Category, Long> {

    Collection<Category> findByRestaurantId(Long restaurantId);
}
