package com.gitlab.foodprojectdemo.restaurantservice.domain.promo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.beans.ConstructorProperties;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
@DiscriminatorValue("percent_promo_action")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PercentDiscountPromoAction extends PromoAction {
    @JsonIgnore
    @Transient
    private final String aggregateName = "percent_promo_action";

    @Type(type = "jsonb")
    @Column(name = "data", columnDefinition = "jsonb")
    List<PercentDiscountItem> discounts;

    @ConstructorProperties({"id", "restaurantId", "discounts"})
    public PercentDiscountPromoAction(Long id, Long restaurantId, Collection<PercentDiscountItem> discounts) {
        setId(id);
        setRestaurantId(restaurantId);
        this.discounts = discounts.stream().sorted().collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "PercentDiscountPromoAction{" +
                " discounts=" + discounts +
                ", id=" + id +
                ", restaurantId=" + restaurantId +
                '}';
    }
}
