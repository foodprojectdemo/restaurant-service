package com.gitlab.foodprojectdemo.restaurantservice.adapter.messaging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidTypeIdException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;


@Slf4j
@Component
@AllArgsConstructor
public class EventListener {
    private final ObjectMapper objectMapper;
    private final Map<
            Class<? extends DomainEventDto<?>>,
            Collection<Consumer<DomainEventDto<?>>>
            > subscription = new ConcurrentHashMap<>();


    @KafkaListener(groupId = "event-listener", topics = "${kafka.topic.events-topic}")
    public void events(String json, Acknowledgment ack) {
        log.info("Received message: " + json);

        var event = parse(json);

        event.ifPresent(this::send);

        ack.acknowledge();
    }

    @SuppressWarnings("unchecked")
    public <E extends DomainEventDto<?>> void subscribe(Class<E> event, Consumer<E> subscriber) {
        subscription.compute(event, (__, consumers) -> {
                    if (consumers == null) {
                        Collection<Consumer<DomainEventDto<?>>> newConsumers = new LinkedList<>();
                        newConsumers.add((Consumer<DomainEventDto<?>>) subscriber);
                        return newConsumers;
                    } else {
                        consumers.add((Consumer<DomainEventDto<?>>) subscriber);
                        return consumers;
                    }
                }
        );
    }

    private void send(DomainEventDto<?> event) {
        log.info("Received event: " + event);
        subscription.get(event.getClass()).forEach(consumer -> consumer.accept(event));
    }

    private Optional<DomainEventDto<?>> parse(String json) {
        try {
            return Optional.of(objectMapper.readValue(json, new TypeReference<>() {
            }));
        } catch (JsonProcessingException e) {
            if (e instanceof InvalidTypeIdException) {
                log.info("Skip unknown message {}", json);
            } else {
                log.error(e.getMessage(), e);
            }
        }
        return Optional.empty();
    }
}
