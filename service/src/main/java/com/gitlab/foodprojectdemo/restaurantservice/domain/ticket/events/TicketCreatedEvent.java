package com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.events;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import lombok.Value;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainEvent;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.Ticket;

@Value
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class TicketCreatedEvent extends DomainEvent<Ticket, OrderId> {

    @Getter
    String type = "ticket_created";

    public TicketCreatedEvent(@NonNull Ticket aggregate) {
        super(aggregate);
    }
}

