package com.gitlab.foodprojectdemo.restaurantservice.domain.misc;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantInfo;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantRegistrationCommand;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantService;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.Restaurateur;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.RestaurateurInfo;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.RestaurateurService;

import java.util.Set;

@Service
@AllArgsConstructor
public class RegistrationService {
    private final RestaurantService restaurantService;
    private final RestaurateurService restaurateurService;

    @Value
    public static class RegistrationCommand {
        @NonNull RestaurateurInfo restaurateur;
        @NonNull RestaurantInfo restaurant;
        @NonNull Set<String> cuisines;

        public RestaurantRegistrationCommand restaurantRegistrationCommand() {
            return new RestaurantRegistrationCommand(restaurant, cuisines);
        }
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Restaurateur registration(RegistrationCommand command) {
        var restaurant = restaurantService.registration(command.restaurantRegistrationCommand());
        return restaurateurService.registration(restaurant.getId(), command.getRestaurateur());
    }

}
