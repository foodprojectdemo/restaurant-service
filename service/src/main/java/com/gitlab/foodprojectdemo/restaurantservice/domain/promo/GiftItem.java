package com.gitlab.foodprojectdemo.restaurantservice.domain.promo;

import lombok.Value;

import java.beans.ConstructorProperties;

@Value
public class GiftItem implements Comparable<GiftItem> {
    Integer amount;
    Long dishId;

    @ConstructorProperties({"amount", "dishId"})
    public GiftItem(int amount, long dishId) {
        assert amount > 0 : "amount must be greater than 0";
        this.amount = amount;
        this.dishId = dishId;
    }

    @Override
    public int compareTo(GiftItem o) {
        return amount.compareTo(o.amount);
    }
}
