package com.gitlab.foodprojectdemo.restaurantservice.domain.promo;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Value;
import lombok.experimental.NonFinal;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = AbsoluteDiscountPromoCode.class, name = "absolute_promo_code"),
        @JsonSubTypes.Type(value = PercentDiscountPromoCode.class, name = "percent_promo_code")
})
@Value
@NonFinal
public abstract class PromoCode {
    String code;
    Long restaurantId;
}