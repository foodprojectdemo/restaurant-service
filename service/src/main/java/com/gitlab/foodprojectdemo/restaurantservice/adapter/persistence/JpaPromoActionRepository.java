package com.gitlab.foodprojectdemo.restaurantservice.adapter.persistence;

import org.springframework.data.repository.CrudRepository;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoActionRepository;

public interface JpaPromoActionRepository extends CrudRepository<PromoAction, Long>, PromoActionRepository {

}
