package com.gitlab.foodprojectdemo.restaurantservice.domain.promo;

import lombok.Value;

import java.beans.ConstructorProperties;

@Value
public class DiscountItem implements Comparable<DiscountItem> {
    Integer amount;
    Integer discount;

    @ConstructorProperties({"amount", "discount"})
    public DiscountItem(Integer amount, Integer discount) {
        assert amount > 0 : "amount must be greater than 0";
        assert discount > 0 : "discount must be greater than 0";
        this.amount = amount;
        this.discount = discount;
    }

    @Override
    public int compareTo(DiscountItem o) {
        return amount.compareTo(o.amount);
    }
}
