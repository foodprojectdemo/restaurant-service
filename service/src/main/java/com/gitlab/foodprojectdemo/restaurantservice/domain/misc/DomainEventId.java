package com.gitlab.foodprojectdemo.restaurantservice.domain.misc;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.util.UUID;

@EqualsAndHashCode
@AllArgsConstructor
public class DomainEventId {
    @NonNull
    private final UUID id;

    public DomainEventId() {
        id = UUID.randomUUID();
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
