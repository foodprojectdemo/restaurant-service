package com.gitlab.foodprojectdemo.restaurantservice.adapter;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.PasswordEncoder;

@Component
@AllArgsConstructor
public class SpringPasswordEncoder implements PasswordEncoder {
    private final org.springframework.security.crypto.password.PasswordEncoder passwordEncoder;

    @Override
    public String encode(CharSequence rawPassword) {
        return passwordEncoder.encode(rawPassword);
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }
}
