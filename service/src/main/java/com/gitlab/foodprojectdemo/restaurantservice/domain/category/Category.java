package com.gitlab.foodprojectdemo.restaurantservice.domain.category;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Delegate;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RootAggregate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;


@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "category", indexes = @Index(name = "restaurantId", columnList = "restaurantId"))
@JsonPropertyOrder({"id", "restaurantId", "name"})
public class Category extends RootAggregate<Category, Long> {
    @Transient
    private final String aggregateName = "category";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    private Long restaurantId;

    @NonNull
    @JsonIgnore
    @Delegate
    private CategoryInfo info;
}
