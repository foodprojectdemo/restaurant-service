package com.gitlab.foodprojectdemo.restaurantservice.adapter.gateway.order;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.Builder;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.Order;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderRepository;
import reactor.core.publisher.Mono;

import java.util.Optional;

@Slf4j
@Component
@EnableConfigurationProperties(OrderGatewayProperties.class)
public class OrderGateway implements OrderRepository {
    private final WebClient webClient;
    private final OrderGatewayProperties properties;

    public OrderGateway(Builder builder, OrderGatewayProperties properties) {
        this.webClient = builder.baseUrl(properties.getBaseUri()).build();
        this.properties = properties;
    }

    @Override
    public Optional<Order> findById(OrderId id) {
        return Optional.ofNullable(webClient.get().uri(properties.getOrderUri(), id)
                .retrieve()
                .bodyToMono(Order.class)
                .onErrorResume(throwable -> Mono.empty())
                .block());
    }

}
