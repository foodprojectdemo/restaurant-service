package com.gitlab.foodprojectdemo.restaurantservice.domain.dish;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.Embeddable;

@Getter
@Setter(AccessLevel.PROTECTED)
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Embeddable
public class DishInfo {
    @NonNull
    private String name;
    @NonNull
    private String description;
    @NonNull
    private Integer amount;
    @NonNull
    private String imageUrl;
}
