package com.gitlab.foodprojectdemo.restaurantservice.adapter.persistence;

import org.springframework.data.repository.CrudRepository;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.Ticket;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.TicketRepository;

public interface JpaTicketRepository extends CrudRepository<Ticket, OrderId>, TicketRepository {
}
