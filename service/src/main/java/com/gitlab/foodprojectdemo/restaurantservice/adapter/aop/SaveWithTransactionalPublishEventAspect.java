package com.gitlab.foodprojectdemo.restaurantservice.adapter.aop;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;
import com.gitlab.foodprojectdemo.restaurantservice.adapter.messaging.DomainEventPublisher;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RootAggregate;

@Slf4j
@Aspect
@Component
@AllArgsConstructor
class SaveWithTransactionalPublishEventAspect {
    private final DomainEventPublisher domainEventPublisher;
    private final TransactionTemplate transactionTemplate;

    @Around("execution(* com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainRepository.save(com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RootAggregate))")
    Object save(ProceedingJoinPoint proceedingJoinPoint) {
        return transactionTemplate.execute(status -> transactionalPublish(proceedingJoinPoint));
    }

    @SneakyThrows
    private Object transactionalPublish(ProceedingJoinPoint proceedingJoinPoint) {
        var aggregate = (RootAggregate<?, ?>) proceedingJoinPoint.getArgs()[0];
        log.info("publishing {} events: {}", aggregate.getAggregateName(), aggregate.getEvents());
        var result = proceedingJoinPoint.proceed();
        domainEventPublisher.publish(aggregate.getEvents());
        return result;
    }
}
