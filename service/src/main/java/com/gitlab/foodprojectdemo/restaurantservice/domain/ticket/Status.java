package com.gitlab.foodprojectdemo.restaurantservice.domain.ticket;

public enum Status {
    NEW {
        @Override
        public Status accept() {
            return ACCEPTED;
        }

        @Override
        public Status reject() {
            return REJECTED;
        }
    },
    ACCEPTED {
        @Override
        public Status accept() {
            return this;
        }

        @Override
        public Status ready() {
            return READY;
        }
    },
    REJECTED {
        @Override
        public Status reject() {
            return this;
        }
    },
    READY {
        @Override
        public Status ready() {
            return this;
        }
    };

    public Status accept() {
        throw new IllegalStateException();
    }

    public Status reject() {
        throw new IllegalStateException();
    }

    public Status ready() {
        throw new IllegalStateException();
    }
}
