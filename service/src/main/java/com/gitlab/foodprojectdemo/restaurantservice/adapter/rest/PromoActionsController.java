package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.AbsoluteDiscountPromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoAction;
import com.gitlab.foodprojectdemo.restaurantservice.domain.promo.PromoActionService;

import java.util.Collection;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/restaurants/{restaurantId}/promo-actions")
public class PromoActionsController {
    private final PromoActionService promoActionService;

    @GetMapping
    public Collection<PromoAction> promoActions(@PathVariable Long restaurantId) {
        return promoActionService.promoActions(restaurantId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public IdResponse<?> create(@RequestBody PromoAction promoAction, @PathVariable Long restaurantId) {
        promoAction.setRestaurantId(restaurantId);
        var promo = promoActionService.create(promoAction);
        return new IdResponse<>(promo.getId());
    }
}
