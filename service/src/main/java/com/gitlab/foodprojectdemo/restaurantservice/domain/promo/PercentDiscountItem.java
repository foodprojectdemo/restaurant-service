package com.gitlab.foodprojectdemo.restaurantservice.domain.promo;

import lombok.Value;

import java.beans.ConstructorProperties;

@Value
public class PercentDiscountItem implements Comparable<PercentDiscountItem> {
    Integer amount;
    Integer percent;

    @ConstructorProperties({"amount", "percent"})
    public PercentDiscountItem(int amount, int percent) {
        assert amount > 0 : "amount must be greater than 0";
        assert percent > 0 && percent < 100 : "percent must be greater than 0 and less than 100";
        this.amount = amount;
        this.percent = percent;
    }

    @Override
    public int compareTo(PercentDiscountItem o) {
        return amount.compareTo(o.amount);
    }
}
