package com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant;

import lombok.NonNull;
import lombok.Value;

import java.util.Set;

@Value
public class RestaurantRegistrationCommand {
    @NonNull RestaurantInfo info;
    @NonNull Set<String> cuisines;
}
