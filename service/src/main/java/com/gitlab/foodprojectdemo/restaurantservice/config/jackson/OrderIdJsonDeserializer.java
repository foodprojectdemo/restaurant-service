package com.gitlab.foodprojectdemo.restaurantservice.config.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.boot.jackson.JsonComponent;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;

import java.io.IOException;

@JsonComponent
class OrderIdJsonDeserializer extends JsonDeserializer<OrderId> {

    @Override
    public OrderId deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        return new OrderId(node.textValue());
    }
}
