package com.gitlab.foodprojectdemo.restaurantservice.adapter.persistence;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.Restaurateur;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur.RestaurateurRepository;

import java.util.Optional;

public interface JpaRestaurateurRepository extends CrudRepository<Restaurateur, Long>, RestaurateurRepository {
    @Override
    @Query("select r from Restaurateur r where r.info.login=:login")
    Optional<Restaurateur> findByLogin(String login);
}
