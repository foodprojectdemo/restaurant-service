package com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine.CuisineRepository;

import java.util.Collection;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RestaurantService {
    private final RestaurantRepository restaurantRepository;
    private final CuisineRepository cuisineRepository;

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Restaurant registration(RestaurantRegistrationCommand command) {
        restaurantRepository.findByName(command.getInfo().getName()).ifPresent(restaurant -> {
            throw new RestaurantNameAlreadyExistsException(restaurant.getName());
        });

        var restaurant = new Restaurant(command.getInfo());

        var cuisines = cuisineRepository.addNotExistCuisines(command.getCuisines());
        restaurant.setCuisines(cuisines);

        return restaurantRepository.save(restaurant);
    }

    @Transactional(readOnly = true)
    public Collection<Restaurant> restaurants() {
        return restaurantRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Restaurant> restaurant(Long restaurantId) {
        return restaurantRepository.findById(restaurantId);
    }
}
