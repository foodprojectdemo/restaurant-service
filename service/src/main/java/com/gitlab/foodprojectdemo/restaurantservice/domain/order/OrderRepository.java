package com.gitlab.foodprojectdemo.restaurantservice.domain.order;

import java.util.Optional;

public interface OrderRepository {
    Optional<Order> findById(OrderId id);
}
