package com.gitlab.foodprojectdemo.restaurantservice.domain.misc;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.CategoryNotFoundException;
import com.gitlab.foodprojectdemo.restaurantservice.domain.category.CategoryRepository;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantNotFoundException;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantRepository;

@Service
@AllArgsConstructor
public class RelationCheckService {
    private final RestaurantRepository restaurantRepository;
    private final CategoryRepository categoryRepository;

    public void checkRestaurantExists(Long restaurantId) {
        restaurantRepository.findById(restaurantId).orElseThrow(() -> new RestaurantNotFoundException(restaurantId));
    }

    public void checkCategoryExists(Long categoryId) {
        categoryRepository.findById(categoryId).orElseThrow(() -> new CategoryNotFoundException(categoryId));
    }
}
