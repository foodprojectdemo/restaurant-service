package com.gitlab.foodprojectdemo.restaurantservice.domain.ticket;

import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RootAggregate;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.events.TicketAcceptedEvent;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.events.TicketCreatedEvent;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.events.TicketReadyEvent;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.events.TicketRejectedEvent;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.Status.NEW;

@Entity
@Table(name = "ticket")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
public class Ticket extends RootAggregate<Ticket, OrderId> {
    @Transient
    private final String aggregateName = "ticket";

    @EmbeddedId
    private OrderId id;

    private Long restaurantId;

    @Enumerated(EnumType.STRING)
    @Type(type = "pgsql_enum")
    private Status status;

    @ElementCollection
    @CollectionTable(name = "ticket_items", joinColumns = @JoinColumn(name = "ticket_id"))
    private Collection<TicketItem> items;

    private Ticket(OrderId id, Long restaurantId, Status status, Collection<TicketItem> items) {
        this.id = id;
        this.restaurantId = restaurantId;
        this.status = status;
        this.items = new ArrayList<>(items);
    }

    public static Ticket create(OrderId id, Long restaurantId, Collection<TicketItem> items) {
        var ticket = new Ticket(id, restaurantId, NEW, items);
        ticket.publishEvent(new TicketCreatedEvent(ticket));
        return ticket;
    }

    public void accept() {
        status = status.accept();
        publishEvent(new TicketAcceptedEvent(this));
    }

    public void reject() {
        status = status.reject();
        publishEvent(new TicketRejectedEvent(this));
    }

    public void ready() {
        status = status.ready();
        publishEvent(new TicketReadyEvent(this));
    }

    public List<TicketItem> getItems() {
        return List.copyOf(items);
    }
}
