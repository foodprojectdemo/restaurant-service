package com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Delegate;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RootAggregate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Getter
@Setter
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "restaurateur")
@JsonIgnoreProperties("password")
public class Restaurateur extends RootAggregate<Restaurateur, Long> {
    @Transient
    private final String aggregateName = "restaurateur";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    private Long restaurantId;

    @NonNull
    @JsonIgnore
    @Delegate
    private RestaurateurInfo info;
}
