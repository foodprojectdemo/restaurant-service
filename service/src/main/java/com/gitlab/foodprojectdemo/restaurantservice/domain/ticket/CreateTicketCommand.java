package com.gitlab.foodprojectdemo.restaurantservice.domain.ticket;

import lombok.NonNull;
import lombok.Value;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;

import java.util.Collection;
import java.util.stream.Collectors;

@Value
public class CreateTicketCommand {

    @Value
    public static class Item {
        long dishId;
        @NonNull String name;
        int quantity;

        TicketItem ticketItem() {
            return new TicketItem(dishId, name, quantity);
        }
    }

    @NonNull OrderId orderId;
    @NonNull Long restaurantId;
    @NonNull Collection<Item> items;

    Collection<TicketItem> getTicketItems() {
        return items.stream().map(Item::ticketItem).collect(Collectors.toList());
    }

    public Ticket createTicket() {
        return Ticket.create(orderId, restaurantId, getTicketItems());
    }
}
