package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.Restaurant;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantNotFoundException;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantService;

import java.util.Collection;

@Slf4j
@RequestMapping(value = "/api/v1/restaurants")
@RestController
@AllArgsConstructor
public class RestaurantsController {
    private final RestaurantService restaurantService;

    @GetMapping
    public Collection<Restaurant> restaurants() {
        return restaurantService.restaurants();
    }

    @GetMapping("/{restaurantId}")
    public Restaurant restaurant(@PathVariable Long restaurantId) {
        return restaurantService.restaurant(restaurantId).orElseThrow(() -> new RestaurantNotFoundException(restaurantId));
    }
}
