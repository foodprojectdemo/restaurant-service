package com.gitlab.foodprojectdemo.restaurantservice.adapter.persistence;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.Restaurant;
import com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant.RestaurantRepository;

import java.util.Optional;

public interface JpaRestaurantRepository extends CrudRepository<Restaurant, Long>, RestaurantRepository {

    @Override
    @Query("select r from Restaurant r where r.info.name=:name")
    Optional<Restaurant> findByName(String name);

}
