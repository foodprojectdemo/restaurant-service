package com.gitlab.foodprojectdemo.restaurantservice.config.jackson;

public class IllegalValueObjectException extends IllegalArgumentException{

    public IllegalValueObjectException(String message, Throwable cause) {
        super(message, cause);
    }
}
