package com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant;

import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainRepository;

import java.util.Collection;
import java.util.Optional;

public interface RestaurantRepository extends DomainRepository<Restaurant, Long> {

    Optional<Restaurant> findByName(String name);

    Collection<Restaurant> findAll();
}
