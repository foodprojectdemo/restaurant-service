package com.gitlab.foodprojectdemo.restaurantservice.config.jackson;

import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.springframework.boot.jackson.JsonComponent;

@JsonComponent
class DomainEventIdJsonSerializer extends ToStringSerializer {

}
