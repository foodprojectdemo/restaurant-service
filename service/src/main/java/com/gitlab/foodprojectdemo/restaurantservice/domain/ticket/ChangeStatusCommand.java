package com.gitlab.foodprojectdemo.restaurantservice.domain.ticket;

import lombok.Value;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;

@Value
public class ChangeStatusCommand {
    OrderId orderId;
    Long restaurantId;
    NewStatus status;

    public void changeStatus(Ticket ticket) {
        status.changeStatus(ticket);
    }
}
