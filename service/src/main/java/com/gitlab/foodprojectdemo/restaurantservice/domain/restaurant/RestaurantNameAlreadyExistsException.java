package com.gitlab.foodprojectdemo.restaurantservice.domain.restaurant;

import com.gitlab.foodprojectdemo.restaurantservice.domain.exception.DomainException;

public class RestaurantNameAlreadyExistsException extends DomainException {

    public RestaurantNameAlreadyExistsException(String name) {
        super(String.format("The name of the restaurant '%s' already exists", name));
    }
}
