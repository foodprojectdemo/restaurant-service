package com.gitlab.foodprojectdemo.restaurantservice.domain.promo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.beans.ConstructorProperties;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
@DiscriminatorValue("gift_promo_action")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GiftPromoAction extends PromoAction {
    @JsonIgnore
    @Transient
    private final String aggregateName = "gift_promo_action";

    @Type(type = "jsonb")
    @Column(name = "data", columnDefinition = "jsonb")
    List<GiftItem> gifts;

    @ConstructorProperties({"id", "restaurantId", "discounts"})
    public GiftPromoAction(Long id, Long restaurantId, Collection<GiftItem> gifts) {
        setId(id);
        setRestaurantId(restaurantId);
        this.gifts = gifts.stream().sorted().collect(Collectors.toList());
    }
}
