package com.gitlab.foodprojectdemo.restaurantservice.domain.promo;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.TypeDef;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RootAggregate;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = AbsoluteDiscountPromoAction.class, name = "absolute_promo_action"),
        @JsonSubTypes.Type(value = PercentDiscountPromoAction.class, name = "percent_promo_action"),
        @JsonSubTypes.Type(value = GiftPromoAction.class, name = "gift_promo_action")
})
@Entity
@Getter
@Setter(AccessLevel.PROTECTED)
@Table(name = "promo_action", indexes = @Index(name = "restaurantId", columnList = "restaurantId"))
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public abstract class PromoAction extends RootAggregate<PromoAction, Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(updatable = false)
    @Setter
    Long restaurantId;
}