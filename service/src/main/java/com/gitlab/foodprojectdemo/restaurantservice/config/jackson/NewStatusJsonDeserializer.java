package com.gitlab.foodprojectdemo.restaurantservice.config.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.boot.jackson.JsonComponent;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.NewStatus;

import java.io.IOException;

@JsonComponent
class NewStatusJsonDeserializer extends JsonDeserializer<NewStatus> {

    @Override
    public NewStatus deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        var status = node.textValue();
        try {
            return NewStatus.valueOf(status.toUpperCase());
        } catch (IllegalArgumentException throwable) {
            throw new IllegalValueObjectException(String.format("Invalid value of status: %s", status), throwable);
        }
    }
}
