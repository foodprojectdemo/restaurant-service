package com.gitlab.foodprojectdemo.restaurantservice.domain.order;

import lombok.Value;
import com.gitlab.foodprojectdemo.restaurantservice.adapter.messaging.DomainEventDto;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainEventId;

import java.time.ZonedDateTime;

@Value
public class OrderRejectedEvent implements DomainEventDto<OrderId> {
    DomainEventId id;
    String aggregate;
    OrderId aggregateId;
    ZonedDateTime occurredOn;
}
