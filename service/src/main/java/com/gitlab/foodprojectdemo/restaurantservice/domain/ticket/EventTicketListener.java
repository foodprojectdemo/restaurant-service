package com.gitlab.foodprojectdemo.restaurantservice.domain.ticket;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.Order;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderPlacedEvent;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderRepository;

import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class EventTicketListener {
    private final OrderRepository orderRepository;
    private final TicketService ticketService;

    public void onEventCreateTicket(OrderPlacedEvent orderPlacedEvent) {
        var order = orderRepository.findById(orderPlacedEvent.getAggregateId());
        if (order.isEmpty()) {
            log.warn("Order {} was not found", orderPlacedEvent);
            return;
        }

        order.map(this::orderToCreateTicketCommand)
                .map(ticketService::create);
    }

    private CreateTicketCommand orderToCreateTicketCommand(Order order) {
        return new CreateTicketCommand(
                order.getId(),
                order.getRestaurantId(),
                order.getItems()
                        .stream()
                        .map(item -> new CreateTicketCommand.Item(item.getDishId(), item.getName(), item.getQuantity()))
                        .collect(Collectors.toList())
        );
    }
}
