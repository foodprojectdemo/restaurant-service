package com.gitlab.foodprojectdemo.restaurantservice.domain.promo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.beans.ConstructorProperties;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@EqualsAndHashCode(callSuper = true)
@Entity
@DiscriminatorValue("absolute_promo_action")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AbsoluteDiscountPromoAction extends PromoAction {
    @Transient
    @JsonIgnore
    private final String aggregateName = "absolute_promo_action";

    @Type(type = "jsonb")
    @Column(name = "data", columnDefinition = "jsonb")
    List<DiscountItem> discounts;

    @ConstructorProperties({"id", "restaurantId", "discounts"})
    public AbsoluteDiscountPromoAction(Long id, Long restaurantId, Collection<DiscountItem> discounts) {
        setId(id);
        setRestaurantId(restaurantId);
        this.discounts = discounts.stream().sorted().collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "AbsoluteDiscountPromoAction{" +
                "id=" + id +
                ", restaurantId=" + restaurantId +
                ", discounts=" + discounts +
                '}';
    }
}
