package com.gitlab.foodprojectdemo.restaurantservice.domain.cuisine;

import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainRepository;

import java.util.Set;

public interface CuisineRepository extends DomainRepository<Cuisine, Long> {

    Set<Cuisine> addNotExistCuisines(Set<String> cuisineNames);

}
