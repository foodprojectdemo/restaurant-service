package com.gitlab.foodprojectdemo.restaurantservice.config;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import com.gitlab.foodprojectdemo.restaurantservice.adapter.messaging.EventListener;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderPlacedEvent;
import com.gitlab.foodprojectdemo.restaurantservice.domain.ticket.EventTicketListener;

import javax.annotation.PostConstruct;

@Component
@AllArgsConstructor
class EventSubscription {
    private final EventListener eventListener;
    private final EventTicketListener eventTicketListener;

    @PostConstruct
    void subscription() {
        eventListener.subscribe(OrderPlacedEvent.class, eventTicketListener::onEventCreateTicket);
    }
}
