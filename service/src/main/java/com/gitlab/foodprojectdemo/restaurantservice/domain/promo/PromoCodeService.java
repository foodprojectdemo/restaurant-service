package com.gitlab.foodprojectdemo.restaurantservice.domain.promo;

import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class PromoCodeService {


    public Collection<PromoCode> promoCodes(Long restaurantId) {
        return List.of(
                new AbsoluteDiscountPromoCode("DISCOUNT_200", restaurantId, 200),
                new PercentDiscountPromoCode("PERCENT_15", restaurantId, 15)
        );
    }

    public Optional<PromoCode> promoCode(Long restaurantId, String code) {
        return List.of(
                new AbsoluteDiscountPromoCode("DISCOUNT_200", restaurantId, 200),
                new PercentDiscountPromoCode("PERCENT_15", restaurantId, 15)
        ).stream().filter(promoCode -> promoCode.getCode().equals(code)).findFirst();
    }
}
