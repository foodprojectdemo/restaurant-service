package com.gitlab.foodprojectdemo.restaurantservice.domain.promo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class PercentDiscountPromoCode extends PromoCode {
    int percent;

    public PercentDiscountPromoCode(String code, Long restaurantId, int percent) {
        super(code, restaurantId);
        assert percent > 0 && percent < 100 : "percent must be greater than 0 and less than 100";
        this.percent = percent;
    }
}
