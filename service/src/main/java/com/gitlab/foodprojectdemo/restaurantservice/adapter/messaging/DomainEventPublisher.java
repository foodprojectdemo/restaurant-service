package com.gitlab.foodprojectdemo.restaurantservice.adapter.messaging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainEvent;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.RootAggregate;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Slf4j
@Component
@RequiredArgsConstructor
public class DomainEventPublisher {
    private final KafkaTemplate<String, String> kafkaTemplate;
    private final ObjectMapper objectMapper;
    @Value("${kafka.topic.events-topic}")
    private String eventTopic;

    public <A extends RootAggregate<A, ID>, ID> void publish(List<DomainEvent<A, ID>> events) {
        events.forEach(this::publish);
    }

    public <A extends RootAggregate<A, ID>, ID> void publish(DomainEvent<A, ID> event) {
        try {
            kafkaTemplate.send(eventTopic, objectMapper.writeValueAsString(event)).get();
        } catch (InterruptedException | JsonProcessingException | ExecutionException e) {
            log.error(e.getMessage(), e);
        }
    }
}
