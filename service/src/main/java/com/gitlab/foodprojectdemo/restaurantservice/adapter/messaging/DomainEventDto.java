package com.gitlab.foodprojectdemo.restaurantservice.adapter.messaging;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainEventId;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderPlacedEvent;

import java.time.ZonedDateTime;

@JsonTypeInfo(use = Id.NAME, property = "type")
@JsonSubTypes(
        @Type(value = OrderPlacedEvent.class, name = "order_placed")
)
public interface DomainEventDto<ID> {
    DomainEventId getId();

    String getAggregate();

    ID getAggregateId();

    ZonedDateTime getOccurredOn();
}
