package com.gitlab.foodprojectdemo.restaurantservice.domain.restaurateur;

import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainRepository;

import java.util.Optional;

public interface RestaurateurRepository extends DomainRepository<Restaurateur, Long> {
    Optional<Restaurateur> findByLogin(String login);
}
