package com.gitlab.foodprojectdemo.restaurantservice.adapter.rest;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.Dish;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.DishInfo;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.DishNotFoundException;
import com.gitlab.foodprojectdemo.restaurantservice.domain.dish.DishService;

import java.util.Collection;

@Slf4j
@RestController
@AllArgsConstructor
public class DishesController {
    private final DishService dishService;

    @PostMapping("/api/v1/restaurants/{restaurantId}/categories/{categoryId}/dishes")
    @ResponseStatus(HttpStatus.CREATED)
    public IdResponse<?> addDish(@PathVariable Long restaurantId, @PathVariable Long categoryId, @RequestBody DishInfo info) {
        Dish dish = dishService.addDish(restaurantId, categoryId, info);
        return new IdResponse<>(dish.getId());
    }

    @GetMapping("/api/v1/restaurants/{restaurantId}/dishes")
    public Collection<Dish> dishes(@PathVariable Long restaurantId) {
        return dishService.dishes(restaurantId);
    }

    @GetMapping("/api/v1/dishes/{dishId}")
    public Dish dish(@PathVariable Long dishId) {
        return dishService.dish(dishId).orElseThrow(() -> new DishNotFoundException(dishId));
    }
}
