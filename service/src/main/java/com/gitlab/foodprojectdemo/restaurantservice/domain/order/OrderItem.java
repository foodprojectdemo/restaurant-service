package com.gitlab.foodprojectdemo.restaurantservice.domain.order;

import lombok.NonNull;
import lombok.Value;

@Value
public class OrderItem {
    long dishId;
    @NonNull String name;
    int quantity;
}
