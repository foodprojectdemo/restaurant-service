package com.gitlab.foodprojectdemo.restaurantservice.domain.ticket;

public enum NewStatus {
    ACCEPTED {
        @Override
        void changeStatus(Ticket ticket) {
            ticket.accept();
        }
    }, REJECTED {
        @Override
        void changeStatus(Ticket ticket) {
            ticket.reject();
        }
    }, READY {
        @Override
        void changeStatus(Ticket ticket) {
            ticket.ready();
        }
    };

    abstract void changeStatus(Ticket ticket);
}
