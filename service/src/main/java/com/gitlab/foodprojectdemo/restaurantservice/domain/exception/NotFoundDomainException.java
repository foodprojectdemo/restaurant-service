package com.gitlab.foodprojectdemo.restaurantservice.domain.exception;

public class NotFoundDomainException extends DomainException {

    public NotFoundDomainException(String message) {
        super(message);
    }
}
