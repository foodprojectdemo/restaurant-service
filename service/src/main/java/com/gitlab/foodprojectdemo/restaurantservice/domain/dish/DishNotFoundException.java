package com.gitlab.foodprojectdemo.restaurantservice.domain.dish;

import com.gitlab.foodprojectdemo.restaurantservice.domain.exception.NotFoundDomainException;

public class DishNotFoundException extends NotFoundDomainException {

    public DishNotFoundException(Long dishId) {
        super(String.format("Dish with ID %d was not found", dishId));
    }
}
