package com.gitlab.foodprojectdemo.restaurantservice.domain.ticket;

import com.gitlab.foodprojectdemo.restaurantservice.domain.misc.DomainRepository;
import com.gitlab.foodprojectdemo.restaurantservice.domain.order.OrderId;

import java.util.List;
import java.util.Optional;

public interface TicketRepository extends DomainRepository<Ticket, OrderId> {

    Optional<Ticket> findByIdAndRestaurantId(OrderId id, Long restaurantId);

    //TODO: add order field datetime
    List<Ticket> findByRestaurantId(Long restaurantId);
}
