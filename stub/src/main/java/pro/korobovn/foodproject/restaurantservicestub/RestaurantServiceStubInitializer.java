package com.gitlab.foodprojectdemo.restaurantservicestub;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.Profiles;

import java.util.Map;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class RestaurantServiceStubInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    private static final String RESTAURANT_STUB_PROFILE_NAME = "restaurant-service-stub";

    @Override
    public void initialize(ConfigurableApplicationContext context) {
        var environment = context.getEnvironment();
        var available = environment.acceptsProfiles(Profiles.of(RESTAURANT_STUB_PROFILE_NAME));

        if (available) {
            var wireMockServer = createWireMock();
            wireMockServer.start();

            overloadRestaurantUrl(context, wireMockServer.port());

            context.addApplicationListener(
                    applicationEvent -> {
                        if (applicationEvent instanceof ContextClosedEvent) {
                            wireMockServer.stop();
                        }
                    });
        }
    }

    private WireMockServer createWireMock() {
        var options = options()
                .dynamicPort()
                .usingFilesUnderClasspath("stubs");

        return new WireMockServer(options);
    }

    private void overloadRestaurantUrl(ConfigurableApplicationContext context, int wireMockPort) {
        Map<String, Object> properties = Map.of("restaurant-service.url", String.format("http://localhost:%d/", wireMockPort));
        context.getEnvironment().getPropertySources().addFirst(new MapPropertySource("restaurant-stub-properties", properties));
    }
}
