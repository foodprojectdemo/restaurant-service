# Restaurant service stub

### Installation

```
<dependency>
    <groupId>com.gitlab.foodprojectdemo</groupId>
    <artifactId>restaurant-service-stub</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
```

Run with profile ``--spring.profiles.active=restaurant-service-stub``